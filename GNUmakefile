#
# GNUmakefile
#
# Copyright (C) Richard Ferreira
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written authorization.
#

include defs.mk

CC = gcc
SYSCONFDIR ?= /etc
LDFLAGS +=
PREFIX ?= /usr/local
SYSCONFDIR ?= $(PREFIX)/etc
INITDIR ?= $(SYSCONFDIR)/init.d
MANDIR ?= $(DATADIR)/man
SBINDIR ?= $(PREFIX)/sbin
BINDIR ?= $(PREFIX)/bin
DATADIR ?= $(PREFIX)/share
OBJECTS = $(SOURCES:.c=.o) $(SOURCES_LINUX:.c=.o)
DEPS = $(OBJECTS:main.o=)

all: $(EXECUTABLE) $(OBJECTS)

$(EXECUTABLE): $(OBJECTS)
	$(CC) -o $(EXECUTABLE) $(OBJECTS) $(LDFLAGS)

%.o: %.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $<

clean:
	rm -f $(EXECUTABLE) $(OBJECTS)

$(EXECUTABLE).o: $(HEADERS) $(DEPS)

hash.o: hash.c hash.h
error.o: error.c error.h strlib.o
strlib.o: strlib.c strlib.h
xstat.o: strlib.o hash.o error.o xstat.c xstat.h
iniconf.o: strlib.o iniconf.c iniconf.h
color.o: strlib.o error.o color.h
main.o: $(DEPS) main.h

titest: titest.o
	$(CC) -o $@ $< $(LDFLAGS)

.PHONY: all clean
