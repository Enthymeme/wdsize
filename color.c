/*
 * color.c
 *
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#include "color.h"
#include "error.h"

int is_term(const char *s, const char *termname)
{
	char *envterm = getenv("TERM");
	char *termz = NULL;
	char *item = NULL;
	int ret = 0;
	size_t tsiz = 0;

	if (!cur_term)
		return 0;
	if (!termname)
		termname = envterm;
	if (!(termz = strtoargz(s, ',', &tsiz)))
		return 0;
	while ((item = argz_get(termz, tsiz, item)) && ret == 0)
		if (!strcmp(item, termname))
			ret = 1;
	free(termz);
	return ret;
}

int setupcolor(int fildes)
{
	char *ttydev;

	if (!cur_term && setupterm(NULL, fildes, NULL) == ERR) {
		error(0, 0, "%s: could not initialize terminal", __func__);
		return -1;
	}
	if (tigetnum("colors") < 0) {
		if (!(ttydev = ttyname(fildes))) {
			error(0, errno, "%s: ttydev", __func__);
			ttydev = "unknown device";
		}
		error(0, 0, "%s: tty \"%s\" (%s) has no colors available",
			__func__, getenv("TERM"), ttydev);
		return -1;
	}
	return 0;
}

void cbfree(struct colorset **cb)
{
	if (!(*cb))
		return;
	if ((*cb)->fg)
		free((*cb)->fg);
	if ((*cb)->bg)
		free((*cb)->bg);
	if ((*cb)->attr)
		free((*cb)->attr);
	if ((*cb)->reset)
		free((*cb)->reset);
	free(*cb);
	*cb = NULL;
}

static int strtoattr(const char *name, long *bit, char **cap)
{
	static const struct {
		const char *name;
		const char *cap;
		int bit;
	} attrmap[] = {
		{ "standout",	"smso",		0	},
		{ "underscore",	"smul",		1	},
		{ "reverse",	"rev",		2	},
		{ "blink",	"blink",	3	},
		{ "dim",	"dim",		4	},
		{ "bold",	"bold",		5	},
		{ "hidden",	"invis",	6	},
		{ "protect",	"prot",		7	},
		{ NULL,		NULL,		-1	}
	};
	int i;
	char *tail;

	for (i = 0; attrmap[i].name; i++) {
		if (!strcmp(name, attrmap[i].name) || !strcmp(name, attrmap[i].cap)) {
			if (bit) {
				bit[attrmap[i].bit] = 1;
				return 1;
			}
			if (*cap) {
				*cap = (char *)attrmap[i].cap;
				return 1;
			}
		}
	}
	/* assume an integer attr if strcmp fails */
	i = strtol(name, &tail, 10);
	if (tail != name && i >= 0 && i <= 7) {
		if (bit) {
			bit[attrmap[i].bit] = 1;
			return 1;
		}
		if (*cap) {
			*cap = (char *)attrmap[i].cap;
			return 1;
		}
	}
	return 0;
}

char *set_attr(const char *attr)
{
	long bit[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	size_t n;
	char *ret = NULL, *attrz = NULL;
	char *cap_string, *attr_name, *cap_name;
	int any_attr = 0;

	if (!attr)
		return NULL;
	attrz = strtoargz(attr, ',', &n);
	attr_name = NULL;
	if (get_string_cap("sgr")) {
		while ((attr_name = argz_get(attrz, n, attr_name)))
			any_attr = strtoattr(attr_name, bit, NULL) || any_attr;
		/* return fuck all if nothing was set, as it should */
		if (any_attr)
			ret = get_cap_parm("sgr", bit[0], bit[1], bit[2], bit[3], bit[4], bit[5], bit[6], bit[7], 0);
	} else {
		ret = calloc(SLBUFSIZ, sizeof(*ret));
		while ((attr_name = argz_get(attrz, n, attr_name))) {
			cap_name = NULL;
			strtoattr(attr_name, NULL, &cap_name);
			if (cap_name && (cap_string = get_cap_parm(cap_name))) {
				strncat(ret, cap_string, bufcatlen(ret, SLBUFSIZ));
				free(cap_string);
				any_attr = 1;
			}
		}
		if (!any_attr) {
			free(ret);
			ret = NULL;
		}
	}
	free(attrz);
	return ret;
}

static int get_color_name(const char *src)
{
	const struct {
		const char *name;
		int value;
	} colormap[] = {
		{ "black",	COLOR_BLACK	},
		{ "red",	COLOR_RED	},
		{ "green",	COLOR_GREEN	},
		{ "yellow",	COLOR_YELLOW	},
		{ "blue",	COLOR_BLUE	},
		{ "magenta",	COLOR_MAGENTA	},
		{ "cyan",	COLOR_CYAN	},
		{ "white",	COLOR_WHITE	},
		{ NULL,		0		}
	};
	int i;
	char *tail;

	if (!src)
		return -1;
	for (i = 0; colormap[i].name; i++) {
		if (!strcmp(src, colormap[i].name))
			return colormap[i].value;
	}
	i = strtol(src, &tail, 10);
	if (tail != src)
		return i;
	return -1;
}

struct colorset *find_out_colors(const char *attr, const char *fg, const char *bg)
{
	struct colorset *cb;
	int bg_color, fg_color;

	if (!(cb = calloc(1, sizeof(*cb))))
		return NULL;
	if (!(cb->reset = get_cap_parm("sgr0"))) {
		cbfree(&cb);
		return NULL;
	}
	cb->attr = set_attr(attr);
	if ((fg_color = get_color_name(fg)) != -1)
		if (!(cb->fg = get_cap_parm("setaf", fg_color)))
			cb->fg = get_cap_parm("setf", fg_color);
	if ((bg_color = get_color_name(bg)) != -1)
		if (!(cb->bg = get_cap_parm("setab", bg_color)))
			cb->bg = get_cap_parm("setb", bg_color);
	if (!(cb->fg || cb->bg || cb->attr)) {
		cbfree(&cb);
		return NULL;
	}
	return cb;
}

struct colorset *get_colorset(const char *s)
{
	char buf[SLBUFSIZ] = "";
	char *attr, *fg, *bg;

	strncpy(buf, s, sizeof(buf) - 1);
	attr = bg = fg = NULL;
	if ((fg = strchr(buf, ';'))) {
		attr = buf;
		*fg = 0;
		fg++;
	} else {
		attr = NULL;
		fg = buf;
	}
	if (fg && (bg = strchr(fg, ','))) {
		*bg = 0;
		bg++;
	}
	if (fg)
		strtrim(fg);
	if (attr)
		strtrim(attr);
	if (bg)
		strtrim(bg);
	return find_out_colors(attr, fg, bg);
}

char *get_cap_parm(const char *capname, ...)
{
	char *tstr, *tp;
	long arg[9] = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
	int i;
	va_list argp;

	if (!(tstr = get_string_cap(capname)))
		return NULL;
	va_start(argp, capname);
	for (i = 0; i < vsizeof(arg); i++)
		arg[i] = va_arg(argp, long);
	va_end(argp);
	tp = tiparm(tstr, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8]);
	return tp ? strdup(tp) : NULL;
}

char *get_string_cap(const char *capname)
{
	char *tstr;

	tstr = tigetstr(capname);
	if (!tstr)
		fprintf(stderr, "%s: \"%s\" capability cancelled or absent\n", __func__, capname);
	if (tstr == (char *)-1) {
		fprintf(stderr, "%s: \"%s\" is not a string cap\n", __func__, capname);
		tstr = NULL;
	}
	return tstr;
}
