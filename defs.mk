#
# Copyright (C) Ludvig Hummel, Richard Ferreira
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
# THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the sale,
# use or other dealings in this Software without prior written authorization.

PACKAGE = wdsize
VERSION = 0.1
EXECUTABLE = wdsize
LDFLAGS += -lncurses -pthread
CPPFLAGS += -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64 -DPACKAGE=\"$(PACKAGE)\" -DVERSION=\"$(VERSION)\" -DSYSCONFDIR=\"$(SYSCONFDIR)\"
CFLAGS += -std=c11 -g3 -O0 -Wall -pedantic -mtune=native -fstrict-aliasing -Werror=format-security -pipe -fgnu89-inline
SOURCES = xstat.c main.c strlib.c hash.c error.c iniconf.c color.c
SOURCES_ETC = titest.c
HEADERS = $(SOURCES:.c=.h)
DIST = $(SOURCES) $(HEADERS) general.mk GNUmakefile LICENSE $(SOURCES_ETC)
