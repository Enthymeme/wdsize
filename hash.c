/*
 * hash.c
 * 
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
 */

#include "hash.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

htable htable_init(
	size_t buckets,
	size_t (*hash)(const void *, size_t),
	int (*match)(const void *, const void *),
	void (*bfree)(void *))
{
	htable t = malloc(sizeof(*t));

	if (!t)
		return NULL;
	if (!(t->bucket = calloc(buckets, sizeof(*t->bucket)))) {
		free(t);
		return NULL;
	}
	t->bcount = buckets;
	t->hash = hash;
	t->match = match;
	t->bfree = bfree;
	t->n_used = 0;
	return t;
}

void htable_fini(htable t)
{
	size_t i;
	hbucket walk, next;

	if (!t)
		return;
	for (i = 0; i < t->bcount; i++) {
		for (walk = t->bucket[i].next; walk; walk = next) {
			if (t->bfree)
				t->bfree(walk->data);
			next = walk->next;
			free(walk);
		}
		if (t->bfree)
			t->bfree(t->bucket[i].data);
	}
	free(t->bucket);
	free(t);
	t = NULL;
	return;
}

static int safe_hasher(size_t *b, size_t (*hash)(const void *, size_t), const void *data, size_t size)
{
	if ((*b = hash(data, size)) >= size) {
		errno = ERANGE;
		return -1;
	}
	return 0;
}

static void bdel(hbucket list, hbucket *item)
{
	hbucket walk = list, prev;

	if (list == *item)
		return;
	while (walk && walk != *item) {
		prev = walk;
		walk = walk->next;
	}
	if (!walk || !prev)
		return;
	prev->next = (*item)->next;
	free(*item);
	*item = prev;
}

static int bpush(hbucket dest, void *data)
{
	hbucket walk, src;

	if (!dest || !data)
		return 0;

	for (walk = dest; walk; walk = walk->next) {
		if (!walk->data) {
			walk->data = data;
			return 0;
		}
	}
	if (!(src = calloc(1, sizeof *src))) {
		errno = ENOMEM;
		return -1;
	}
	src->data = data;
	for (walk = dest; walk->next; walk = walk->next);
	walk->next = src;
	return 0;
}

int htable_insert(htable t, void *data)
{
	size_t b;
	struct hbucket *newp;

	if (!t) {
		errno = EINVAL;
		return -1;
	}
	if (htable_lookup(t, data))
		return 1;
	if (t->n_used >= t->bcount - 1) {
		newp = realloc(t->bucket, sizeof(*t->bucket) * (t->bcount * GROWTH_FACTOR));
		if (!newp) {
			errno = ENOMEM;
			return -1;
		}
		t->bucket = newp;
		memset(&t->bucket[t->bcount], 0, sizeof(*t->bucket) * (t->bcount * GROWTH_FACTOR - t->bcount));
		t->bcount *= GROWTH_FACTOR;
		return htable_insert(t, data);
	}
	if (safe_hasher(&b, t->hash, data, t->bcount) < 0 || bpush(&t->bucket[b], data) < 0)
		return -1;
	t->n_used++;
	return 0;
}

void *htable_lookup(htable t, void *data)
{
	size_t b;
	struct hbucket *walk;

	if (!t) {
		errno = EINVAL;
		return NULL;
	}
	if (safe_hasher(&b, t->hash, data, t->bcount) < 0)
		return NULL;
	for (walk = &t->bucket[b]; walk; walk = walk->next)
		if (walk->data && t->match(data, walk->data))
			return walk->data;
	return NULL;
}

int htable_delete(htable t, void *data)
{
	size_t b;
	struct hbucket *walk;

	if (!t) {
		errno = EINVAL;
		return -1;
	}
	if (safe_hasher(&b, t->hash, data, t->bcount) < 0)
		return -1;
	for (walk = &t->bucket[b]; walk; walk = walk->next) {
		if (!walk->data)
			continue;
		if (t->match(data, walk->data)) {
			if (t->bfree) {
				t->bfree(walk->data);
				walk->data = NULL;
			}
			bdel(&t->bucket[b], &walk);
			(!t->bucket[b].data && t->n_used > 0) ? t->n_used-- : 0;
			return 1;
		}
	}
	return 0;
}
