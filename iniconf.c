/*
 * iniconf.c
 *
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/
#include "strlib.h"
#include "iniconf.h"
#include "error.h"
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <fnmatch.h>

static void ini_key_free(struct ini_key **kv);

static int issection(char *s)
{
	char *lb;
	char *rb;

	if (isempty(s) || *(lb = s) != '[')
		return 0;
	if (!(rb = strnqchr(s, ']')))
		return 0;
	/* skip empty section headers */
	if (rb - lb == 1)
		return 0;
	return 1;
}

static int strip_brackets(int c)
{
	return c == '[' || c == ']' || iscntrl(c) || isspace(c);
}

static int fnmatchv(const char *patterns, const char *string, int flags)
{
	size_t dircount = 0;
	int ret;
	char *dirpaths;
	char *dir = NULL;
	char *nohome;

	if (!strnqchr(patterns, ':') || !(dirpaths = strtoargz(patterns, ':', &dircount))) {
		if (!(nohome = expand_tilde(patterns)))
			return fnmatch(patterns, string, flags);
		ret = fnmatch(nohome, string, flags);
		free(nohome);
		return ret;
	}
	while ((dir = argz_get(dirpaths, dircount, dir))) {
		nohome = expand_tilde(dir);
		if (fnmatch(nohome ? nohome : dir, string, flags) == 0) {
			free(dirpaths);
			free(nohome);
			return 0;
		}
		free(nohome);
	}
	free(dirpaths);
	return FNM_NOMATCH;
}

static int pattern_match(const char *buf, const char *pattern, int flags)
{
	int cflags = REG_EXTENDED;
	int (*cmpfunc)(const char *, const char *) = strcmp;
	const char *want_regex = NULL;

	if (flags & INI_ICASE) {
		cmpfunc = strcasecmp;
		cflags |= REG_ICASE;
	}
	if ((flags & INI_REGEX) && *pattern == ':')
		want_regex = pattern + 1;
	if (flags & INI_FORCE_REGEX)
		want_regex = pattern;
	if ((flags & INI_STRCMP && cmpfunc(pattern, buf) == 0) ||
	   (want_regex && regmatch(want_regex, buf, cflags) == 0) ||
	   (flags & INI_GLOB && !want_regex && fnmatchv(pattern, buf, 0) == 0))
	{
		return 1;
	}
	return 0;
}

struct ini_key *ini_key_get(ini_section sb, const char *name)
{
	struct ini_key *kv = sb->contents;

	while (kv && strcmp(name, kv->name))
		kv = kv->next;
	return kv;
}

ini_section ini_section_get_last(inistream ib)
{
	ini_section sb = ib->data;

	while (sb && sb->next)
		sb = sb->next;
	return sb;
}

ini_section ini_section_get(inistream ib, const char *section, int flags)
{
	ini_section sb = ib->data;

	while (sb && !pattern_match(section, sb->header, flags ? flags : ib->flags))
		sb = sb->next;
	return sb;
}

struct ini_key *ini_key_get_next(ini_section sb, const char *name, struct ini_key *entry)
{
	if (!sb)
		return NULL;
	if (name) {
		if (!entry)
			return ini_key_get(sb, name);
		entry = entry->next;
		while (entry && strcmp(name, entry->name) != 0)
			entry = entry->next;
	} else {
		if (!entry)
			return sb->contents;
		entry = entry->next;
	}
	return entry;
}

ini_section ini_section_get_next(inistream ib, const char *section, int flags, ini_section entry)
{
	if (!entry && section)
		return ini_section_get(ib, section, flags);
	return entry ? entry->next : ib->data;
}

ini_section ini_section_from_stream(inistream ib, const char *query, int flags)
{
	memset(ib->buf, 0, sizeof(ib->buf));
	do {
		strtrim(ib->buf);
		if (!(isempty(ib->buf) || iscomment(ib->buf)) && issection(ib->buf)) {
			strbtrim(ib->buf, strip_brackets);
			if (!query || pattern_match(query, ib->buf, flags ? flags : ib->flags))
				return ini_section_new(ib, ib->buf);
		}
	} while (fgets(ib->buf, sizeof(ib->buf) - 1, ib->stream) && !ferror(ib->stream));
	return NULL;
}

ini_section ini_merge(ini_section dest, ini_section source)
{
	struct ini_key *kv = NULL;

	while ((kv = ini_key_get_next(source, NULL, kv)))
		ini_key_new(dest, kv->name, kv->value);
	return dest;
}

static int isscape(int c, const char *list)
{
	if (!c)
		return 0;
	do {
		if (*list == c)
			return 1;
	} while (*++list);
	return 0;
}

static void ini_unscape(char *buf, const char *c)
{
	char *s = buf;

	while ((s = strchr(s, '\\')) && isscape(*(s + 1), c))
		memmove(s, s + 1, strlen(s));
}

/* why do I do this to myself */
static char *ini_escape(const char *src, const char *c)
{
	char *ret = malloc(INIBUFSIZ);
	char *p = ret;

	do {
		if (isscape(*src, c))
			*p++ = '\\';
		*p++ = *src;
	} while (*++src);
	*p = 0;
	return ret;
}

struct ini_key *ini_key_from_stream(inistream ib, ini_section sb)
{
	struct ini_key *kv = NULL;
	char *name, *value;

	while (fgets(ib->buf, sizeof(ib->buf) - 1, ib->stream) && !feof(ib->stream)) {
		name = value = NULL;
		if (issection(ib->buf)) {
			fseeko(ib->stream, strlen(ib->buf) * -1, SEEK_CUR);
			return NULL;
		}
		strtrim(ib->buf);
		/* allow brackets in keys only if escaped */
		ini_unscape(ib->buf, "[]\\");
		if (isempty(ib->buf) || iscomment(ib->buf) || !(value = strchr(ib->buf, '=')))
			continue;
		name = ib->buf;
		*value = 0;
		value++;
		strtrim(value);
		strtrim(name);
		kv = ini_key_get(sb, name);
		if (kv && !(ib->flags & INI_REPEAT))
			ini_key_del(sb, kv);
		kv = ini_key_new(sb, name, value);
	}
	return kv;
}

ini_section ini_read_all(inistream ib, const char *section, int flags)
{
	ini_section sb = NULL;

	rewind(ib->stream);
	if (section && (sb = ini_section_get(ib, section, flags)))
		return sb;
	while ((sb = ini_section_from_stream(ib, section, flags)))
		ini_key_from_stream(ib, sb);
	return sb;
}

ini_section ini_section_new(inistream ib, const char *section)
{
	ini_section sb = ib->data;
	ini_section ret = NULL;

	if ((ret = ini_section_get(ib, section, 0)))
		return ret;
	if (!(ret = calloc(1, sizeof(*ret))))
		return NULL;
	ret->header = strdup(section);
	if (!ib->data) {
		ib->data = ret;
		return ret;
	}
	while (sb->next)
		sb = sb->next;
	sb->next = ret;
	return ret;
}

ini_section ini_read(inistream ib, const char *section, int flags)
{
	ini_section sb = NULL;

	if (!section)
		return ini_read_all(ib, section, flags);
	rewind(ib->stream);
	if ((sb = ini_section_get(ib, section, flags)))
		return sb;
	if ((sb = ini_section_from_stream(ib, section, flags)))
		ini_key_from_stream(ib, sb);
	return sb;
}

static int sanity_checks(const char *const fpath, const char *const mode)
{
	int sr;
	struct stat sb;

	sr = stat(fpath, &sb);
	if (sr == 0) {
		/* no directories */
		if (S_ISDIR(sb.st_mode)) {
			errno = EISDIR;
			return -1;
		}
		/* no empty files (for reading) */
		if (sb.st_size == 0 && !strcmp(mode, "r")) {
			errno = EINVAL;
			return -1;
		}
	}
	if (sr == -1) {
		/* any other error */
		if (errno != ENOENT)
			return -1;
		/* ENOENT + read-only */
		if (!strcmp(mode, "r")) {
			errno = EINVAL;
			return -1;
		}
	}
	return 0;
}

static const char *const get_mode(const char *fpath, int flags)
{
	if ((flags & INI_CREAT) && fpath)
		return access(fpath, F_OK) < 0 || (flags & INI_TRUNC) ? "w+" : "r+";
	else if (flags & INI_APPEND && !(flags & INI_WRITE))
		return (flags & INI_TRUNC) ? "w+" : "a+";
	else if (flags & INI_WRITE)
		return (flags & INI_TRUNC) ? "w+" : "r+";
	return "r";
}

inistream ini_open(const char *fpath, int flags)
{
	inistream ret;
	const char *const mode = get_mode(fpath, flags);

	if (sanity_checks(fpath, mode) < 0)
		return NULL;
	if (!(ret = calloc(1, sizeof(*ret))))
		return NULL;
	ret->flags = flags;
	if (!(ret->stream = fopen(fpath, mode))) {
		free(ret);
		return NULL;
	}
	return ret;
}

inistream ini_reopen(const char *fpath, int flags, inistream ib)
{
	inistream ret = calloc(1, sizeof(*ret));
	ini_section sb_old = NULL;
	ini_section sb_new;
	struct ini_key *kv = NULL;
	const char *const mode = get_mode(fpath, flags);

	if (!ret)
		return NULL;
	if (sanity_checks(fpath, mode) < 0) {
		free(ret);
		return NULL;
	}
	if (!(ret->stream = freopen(fpath, mode, ib->stream))) {
		free(ret);
		return NULL;
	}
	ib->stream = NULL;
	while ((sb_old = ini_section_get_next(ib, NULL, 0, sb_old))) {
		sb_new = ini_section_new(ret, sb_old->header);
		while ((kv = ini_key_get_next(sb_old, NULL, kv)))
			ini_key_new(sb_new, kv->name, kv->value);
	}
	ini_close(&ib);
	return ret;
}

static void nltrim(FILE *stream)
{
	int c = 0;
	off_t rollback = -1;

	do {
		fseeko(stream, rollback--, SEEK_END);
	} while ((c = fgetc(stream)) == '\n');
	fseeko(stream, 1, SEEK_CUR);
}

static int ini_flock(inistream ib, int type)
{
	struct flock wlock = { F_WRLCK, SEEK_SET, 0, 0, getpid() };

	if (!(ib->flags & INI_WRLOCK))
		return 0;
	wlock.l_type = type;
	return fcntl(fileno(ib->stream), F_SETLKW, &wlock);
}

int ini_append_to_stream(inistream ib, ini_section sb)
{
	struct ini_key *kv = NULL;
	ini_section sb_lookup;

	if ((ib->flags & INI_EXIST) && (sb_lookup = ini_section_get(ib, sb->header, 0)) != sb) {
		errno = ENOENT;
		return -1;
	}
	if (!(ib->flags & INI_APPEND) && !(ib->flags & INI_WRITE)) {
		errno = EINVAL;
		return -1;
	}
	fseeko(ib->stream, 0, SEEK_END);
	if (ini_flock(ib, F_WRLCK) < 0) {
		error(0, errno, "%s: ini_lock", __func__);
		return -1;
	}
	nltrim(ib->stream);
	putc('\n', ib->stream);
	if (fprintf(ib->stream, "[%s]\n", sb->header) < 0)
		return -1;
	while ((kv = ini_key_get_next(sb, NULL, kv)))
		if (fprintf(ib->stream, "%s=%s\n", kv->name, kv->value) < 0)
			return -1;
	if (fflush(ib->stream) < 0) {
		ini_flock(ib, F_UNLCK);
		error(0, errno, "%s", __func__);
		return -1;
	}
	ini_flock(ib, F_UNLCK);
	rewind(ib->stream);
	return 0;
}

static void jump_to_next_section(inistream ib)
{
	while (fgets(ib->buf, sizeof(ib->buf), ib->stream))
		if (issection(ib->buf)) {
			fseeko(ib->stream, strlen(ib->buf) * -1, SEEK_CUR);
			return;
		}
}

static int nlfputs(const char *s, char *c, FILE *stream)
{
	int ret = 0;

	if (*c != '\n' || *s != '\n')
		ret = fputs(s, stream);
	*c = *s;
	return ret;
}

int ini_write_to_stream(inistream ib, ini_section sb)
{
	FILE *tmp;
	struct ini_key *kv = NULL;
	ini_section sb_lookup;
	off_t where;
	char *escaped_key, *escaped_value;
	char c = 0;
	int e;

	if ((ib->flags & INI_EXIST) && (sb_lookup = ini_section_get(ib, sb->header, 0)) != sb) {
		errno = ENOENT;
		return -1;
	}
	if (!(ib->flags & INI_WRITE)) {
		if (ib->flags & INI_APPEND)
			return ini_append_to_stream(ib, sb);
		else {
			errno = EINVAL;
			return -1;
		}
	}
	rewind(ib->stream);
	if (!ini_section_from_stream(ib, sb->header, 0))
		return ini_append_to_stream(ib, sb);
	where = ftello(ib->stream);
	if (!(tmp = tmpfile())) {
		error(0, errno, "%s: tmpfile", __func__);
		return -1;
	}
	rewind(ib->stream);
	while (fgets(ib->buf, sizeof(ib->buf), ib->stream) && ftello(ib->stream) < where)
		nlfputs(ib->buf, &c, tmp);
	if (fprintf(tmp, "[%s]\n", sb->header) < 0)
		return -1;
	while ((kv = ini_key_get_next(sb, NULL, kv))) {
		escaped_key = ini_escape(kv->name, "[]");
		escaped_value = ini_escape(kv->value, "[]");
		e = fprintf(tmp, "%s=%s\n", escaped_key, escaped_value);
		free(escaped_key);
		free(escaped_value);
		if (e < 0)
			return -1;
	}
	jump_to_next_section(ib);
	if (!feof(ib->stream)) {
		if ((c = fputc('\n', tmp)) == EOF)
			return -1;
	}
	while (fgets(ib->buf, sizeof(ib->buf), ib->stream))
		nlfputs(ib->buf, &c, tmp);
	if (fflush(tmp) < 0) {
		fclose(tmp);
		return -1;
	}
	if (!(ib->stream = freopen(NULL, "w+", ib->stream)))
		return -1;
	if (ini_flock(ib, F_WRLCK) < 0) {
		error(0, errno, "%s: ini_flock", __func__);
		return -1;
	}
	rewind(tmp);
	while (fgets(ib->buf, sizeof(ib->buf), tmp))
		nlfputs(ib->buf, &c, ib->stream);
	if (fflush(ib->stream) < 0) {
		ini_flock(ib, F_UNLCK);
		fclose(tmp);
		return -1;
	}
	rewind(ib->stream);
	ini_flock(ib, F_UNLCK);
	fclose(tmp);
	return 0;
}

struct ini_key *ini_key_newf(ini_section sb, const char *name, const char *value, ...)
{
	va_list argp;
	char *buf;
	struct ini_key *ret;

	if (!(buf = calloc(INIBUFSIZ, sizeof(char))))
		return NULL;
	va_start(argp, value);
	vsnprintf(buf, INIBUFSIZ - 1, value, argp);
	va_end(argp);
	ret = ini_key_new(sb, name, buf);
	free(buf);
	return ret;
}

struct ini_key *ini_key_new(ini_section sb, const char *name, const char *value)
{
	struct ini_key *kv;
	struct ini_key *kv_new;

	if (!sb || !name || !*name) {
		errno = EINVAL;
		return NULL;
	}
	if (!(kv_new = calloc(1, sizeof(*kv))))
		return NULL;
	kv_new->name = strdup(name);
	kv_new->value = strdup(value);
	if (!sb->contents) {
		sb->contents = kv_new;
		return kv_new;
	}
	kv = sb->contents;
	while (kv->next)
		kv = kv->next;
	kv->next = kv_new;
	return kv_new;
}

static void ini_key_free(struct ini_key **kv)
{
	struct ini_key *p;

	while ((p = *kv)) {
		(*kv) = (*kv)->next;
		free(p->name);
		free(p->value);
		free(p);
	}
	free(*kv);
}

struct ini_key *ini_key_del(ini_section sb, struct ini_key *kv)
{
	struct ini_key *cur = sb->contents;
	struct ini_key *prev = NULL;

	while (cur && cur != kv) {
		prev = cur;
		cur = cur->next;
	}
	if (prev)
		prev->next = cur->next;
	if (cur == sb->contents)
		sb->contents = cur->next;
	free(cur->name);
	free(cur->value);
	free(cur);
	return prev;
}

ini_section ini_section_del(inistream ib, ini_section sb)
{
	ini_section cur = ib->data;
	ini_section prev = NULL;

	while (cur && cur != sb) {
		prev = cur;
		cur = cur->next;
	}
	if (prev)
		prev->next = cur->next;
	if (cur == ib->data)
		ib->data = cur->next;
	free(cur->header);
	ini_key_free(&cur->contents);
	free(cur);
	return prev;
}

void ini_close(inistream *ib)
{
	ini_section sb;

	if (!*ib)
		return;
	if ((*ib)->stream)
		fclose((*ib)->stream);
	while ((sb = (*ib)->data)) {
		(*ib)->data = (*ib)->data->next;
		free(sb->header);
		ini_key_free(&sb->contents);
		free(sb);
	}
	free(*ib);
}
