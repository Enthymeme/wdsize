/*
 * iniconf.h
 * 
 * Copyright (C) Richard Ferreira
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#ifndef INICONF_H
#define INICONF_H

#include <stdio.h>

#define INIBUFSIZ	8192

/* these structures weren't meant to be opaque, but they are now */
typedef struct ini_section {
	char *header;
	struct ini_key {
		char *name;
		char *value;
		struct ini_key *next;
	} *contents;
	struct ini_section *next;
} *ini_section;

typedef struct {
	FILE *stream;
	char buf[INIBUFSIZ];
	ini_section data;
	int flags;
} *inistream;

/* flags for ini_open */

/* match sections with strcmp() */
#define INI_STRCMP		(1 << 0)

/* match sections with POSIX ERE (when prepended with a colon) */
#define INI_REGEX		(1 << 1)

/* match sections with glob */
#define INI_GLOB		(1 << 2)

/* case-insensitive matching */
#define INI_ICASE		(1 << 3)

/* read duplicate pairs (default is to skip them) */
#define INI_REPEAT		(1 << 5)

/* parse strings as regex even if section doesn't start with a colon */
#define INI_FORCE_REGEX		(1 << 6)

/* create a new file if it doesn't exist */
#define INI_CREAT		(1 << 7)

/* truncate an existing file */
#define INI_TRUNC		(1 << 8)

/* open file for reading, writing and appending */
#define INI_WRITE		(1 << 9)

/* open for reading and appending only */
#define INI_APPEND		(1 << 10)

/* employ write locks */
#define INI_WRLOCK		(1 << 11)

/* write/append only if section belongs to stream */
#define INI_EXIST		(1 << 12)

/* specify at least one of the STRCMP/GLOB/REGEX flags, there's no default */
#define INI_TRY_ALL 	(INI_STRCMP|INI_GLOB|INI_REGEX)


/* NOTE: only ini_write_to_stream and ini_append_to_stream modify the file on disk.
 * all other functions operate in memory only
 * neither will check if sb belongs to inistream, unless INI_EXIST is used
 */

/* Write a new section to the end of the file */
int ini_append_to_stream(inistream ib, ini_section sb);

/* Write new (i.e., call ini_append if section doesn't exist in the file) or replace an already existing section */
int ini_write_to_stream(inistream ib, ini_section sb);

/* open a new file. flags described above */
inistream ini_open(const char *fpath, int flags);

/* read the first matching section. if section is NULL, load all sections into memory */
ini_section ini_read(inistream ib, const char *section, int flags);

/* read all matching sections. duplicate sections are merged into a single one */
ini_section ini_read_all(inistream ib, const char *section, int flags);

/* merge two unrelated sections. source is kept in the stream */
ini_section ini_merge(ini_section dest, ini_section source);

/* add a new key/value pair to a section. new_keyf accepts printf-style formatting */
struct ini_key *ini_key_new(ini_section sb, const char *name, const char *value);
struct ini_key *ini_key_newf(ini_section sb, const char *name, const char *value, ...);

/* delete a key/value pair from section */
struct ini_key *ini_key_del(ini_section sb, struct ini_key *kv);

/* find a key/value pair */
struct ini_key *ini_key_get(ini_section sb, const char *name);

/* find all matching keys (if name is not NULL), otherwise iterate through all existing keys */
struct ini_key *ini_key_get_next(ini_section sb, const char *name, struct ini_key *entry);

/* find section in memory.
 * flags are passed directly to pattern_match(). 0 means use flags from inistream */
ini_section ini_section_get(inistream ib, const char *section, int flags);

/* read all name/value pairs from stream */
struct ini_key *ini_key_from_stream(inistream ib, ini_section sb);

/* read section from stream */
ini_section ini_section_from_stream(inistream ib, const char *query, int flags);

/* iterate through all sections in inistream, starting at section */
ini_section ini_section_get_next(inistream ib, const char *section, int flags, ini_section entry);

/* get the last section (memory-ordered) in stream */
ini_section ini_section_get_last(inistream ib);

/* create a new empty section */
ini_section ini_section_new(inistream ib, const char *section);

/* delete section and all keys associated with it */
ini_section ini_section_del(inistream ib, ini_section sb);

/* close stream and free all allocated memory */
void ini_close(inistream *ib);

#endif
