/*
 * main.c
 *
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#include "strlib.h"
#include "error.h"
#include "iniconf.h"
#include "main.h"
#include <locale.h>
#include <signal.h>
#include <term.h>
#include <curses.h>
#include <sys/shm.h>
#include <time.h>
#include <pthread.h>
#include <grp.h>
#include <pwd.h>
#include <fnmatch.h>

#ifdef __linux__
# define CLOCK_SOURCE CLOCK_MONOTONIC_RAW
#else
# define CLOCK_SOURCE CLOCK_MONOTONIC
#endif

const char *program_name = PACKAGE;

struct fmtinfo {
	const struct xstat *xb;
	const struct fsinfo *f;
	const char *dirpath;
	uint64_t blocksize;
};

const struct {
	const char *key;
	status_t (*parser)(const char *, struct wbargs *);
	const char *env;
	int scope;
} wdconfig[] = {
	{ "recursive",			wdcset_recursive		,	"WDSIZE_RECURSIVE" 	,	SCOPE_ALL	},
	{ "enabled",			wdcset_enabled			,	"WDSIZE_ENABLED"	,	SCOPE_ALL	},
	{ "xdev",			wdcset_xdev			,	"WDSIZE_XDEV"		,	SCOPE_ALL	},
	{ "format",			wdcset_format			,	"WDSIZE_FORMAT"		,	SCOPE_ALL	},
	{ "color",			wdcset_color			,	"WDSIZE_COLOR"		,	SCOPE_ALL	},
	{ "skip_fs",			wdcset_skip_fs			,	"WDSIZE_SKIP_FS"	,	SCOPE_GLOBAL	},
	{ "skip_fs_opts",		wdcset_skip_fs_opts		,	"WDSIZE_SKIP_FS_OPTS"	,	SCOPE_GLOBAL	},
	{ "skip_devices",		wdcset_skip_devices		,	"WDSIZE_SKIP_DEVICES"	,	SCOPE_GLOBAL	},
	{ "timeout",			wdcset_timeout			,	"WDSIZE_TIMEOUT"	,	SCOPE_ALL	},
	{ "blocksize",			wdcset_blocksize		,	"WDSIZE_BLOCKSIZE"	,	SCOPE_ALL	},
	{ "noenv",			wdcset_noenv			,	NULL			,	SCOPE_ALL	},
	{ "noprogress",			wdcset_noprogress		,	"WDSIZE_NOPROGRESS"	,	SCOPE_ALL	},
	{ "subdirs",			wdcset_subdirs			,	"WDSIZE_SUBDIRS"	,	SCOPE_LOCAL	},
	{ NULL,				NULL				,	NULL			,	0		}
};

struct wbtag {
	const char *name;
	char *(*get_tag_value)(struct stat *, struct fsinfo *);
} tag[] = {
	{ "fs",		get_fs_value 	},
	{ "dev",	get_dev_value	},
	{ "uid",	get_uid_value	},
	{ "gid",	get_gid_value	},
	{ "user",	get_user_value	},
	{ "group",	get_group_value	},
	{ "term",	get_term_value	},
	{ "color",	NULL 		},
	{ NULL,		NULL		}
};

struct progress_caps {
	char *cr;
	char *el;
};

status_t wdcset_xdev(const char *value, struct wbargs *wb)
{
	int i = strbool(value);

	set_flag(wb->xtconf.flags, i, XSTAT_XDEV);
	return STATUS_CARRYON;
}

status_t wdcset_recursive(const char *value, struct wbargs *wb)
{
	char buf[SLBUFSIZ];
	char *subdirs;

	strncpy(buf, value, sizeof(buf));
	stolower(buf);
	if ((subdirs = strchr(buf, '+'))) {
		*subdirs = 0;
		subdirs++;
	}
	if (!strcmp(buf, "always") || strbool(buf) == 1)
		set_flag(wb->xtconf.flags, 1, XSTAT_RECURSIVE);
	else if (!strcmp(buf, "never") || strbool(buf) == 0)
		set_flag(wb->xtconf.flags, 0, XSTAT_RECURSIVE);
	else
		subdirs = buf;
	if (subdirs) {
		if (!strcmp(subdirs, "subdirs"))
			set_flag(wb->xtconf.flags, 1, XSTAT_SUBDIRS);
		else if (!strcmp(subdirs, "nosubdirs"))
			set_flag(wb->xtconf.flags, 0, XSTAT_SUBDIRS);
	}
	return STATUS_CARRYON;
}

status_t wdcset_subdirs(const char *value, struct wbargs *wb)
{
	wb->xtconf.subdir_count = 0;
	wb->xtconf.subdirs = strtoargz(value, ':', &wb->xtconf.subdir_count);
	return STATUS_CARRYON;
}

status_t wdcset_noprogress(const char *value, struct wbargs *wb)
{
	int i = strbool(value);

	if (i < 0) {
		fprintf(stderr, "%s: invalid value: %s", __func__, value);
		return STATUS_CARRYON;
	}
	set_flag(wb->flags, i, WDSIZE_NOPROGRESS);
	return STATUS_CARRYON;
}

status_t wdcset_noenv(const char *value, struct wbargs *wb)
{
	int i = strbool(value);

	if (i < 0) {
		fprintf(stderr, "%s: invalid value: %s", __func__, value);
		return STATUS_CARRYON;
	}
	set_flag(wb->flags, i, WDSIZE_NOENV);
	return STATUS_CARRYON;
}

/* https://misc.flogisoft.com/bash/tip_colors_and_formatting */
status_t wdcset_color(const char *value, struct wbargs *wb)
{
	free(wb->color_scheme);
	cbfree(&wb->cb);
	wb->color_scheme = isempty(value) ? NULL : strdup(value);
	return STATUS_CARRYON;
}

status_t wdcset_enabled(const char *value, struct wbargs *wb)
{
	int enable = strbool(value);

	(void)wb;
	return enable == 0 ? STATUS_DISABLE : STATUS_CARRYON;
}

status_t wdcset_format(const char *value, struct wbargs *wb)
{
	if (wb->fmt)
		free(wb->fmt);
	return (wb->fmt = strdup(value)) == NULL ? STATUS_DISABLE : STATUS_CARRYON;
}

status_t wdcset_skip_fs(const char *value, struct wbargs *wb)
{
	size_t n;
	status_t ret = STATUS_CARRYON;
	char *fslist;
	char *item = NULL;

	if (!(wb->f && wb->f->fstype))
		return STATUS_CARRYON;
	fslist = strtoargz(value, ',', &n);
	while ((item = argz_get(fslist, n, item)) && ret == STATUS_CARRYON)
		if (fnmatch(item, wb->f->fstype, 0) == 0)
			ret = STATUS_DISABLE;
	free(fslist);
	return ret;
}

status_t wdcset_skip_fs_opts(const char *value, struct wbargs *wb)
{
	size_t n, fn = 0;
	status_t ret = STATUS_CARRYON;
	char *optlist, *foptlist;
	char *o_item = NULL;
	char *f_item = NULL;

	if (!(wb->f && wb->f->fsopts))
		return STATUS_CARRYON;
	optlist = strtoargz(value, ',', &n);
	foptlist = strtoargz(wb->f->fsopts, ',', &fn);
	while ((o_item = argz_get(optlist, n, o_item))) {
		while ((f_item = argz_get(foptlist, fn, f_item))) {
			if (!strcmp(f_item, o_item)) {
				ret = STATUS_DISABLE;
				goto out;
			}
		}
	}
out:
	free(optlist);
	free(foptlist);
	return ret;
}

/* I can't use libblkid here because
 *
 * 1) it won't work without a cache, unless root;
 * 2) it needs root to make a cache
 */
#ifdef __linux__
char *get_real_dev(const char *query)
{
	int i;
	char buf[SLBUFSIZ] = "";
	const char *value;
	const char *subdir = NULL;
	const struct {
		const char *tag;
		const char *dir;
	} taglist[] = {
		{ TAG_LABEL,		TAG_LABEL_DIR		},
		{ TAG_PARTLABEL,	TAG_PARTLABEL_DIR	},
		{ TAG_PARTUUID,		TAG_PARTUUID_DIR	},
		{ TAG_UUID,		TAG_UUID_DIR		},
		{ TAG_ID, 		TAG_ID_DIR		},
		{ NULL,			NULL			}
	};
	for (i = 0; taglist[i].tag && !subdir; i++)
		if (!strncmp(query, taglist[i].tag, strlen(taglist[i].tag)))
			subdir = taglist[i].dir;
	if (!(subdir && (value = strchr(query, '='))))
		return NULL;
	value++;
	snprintf(buf, sizeof(buf) - 1, "/dev/disk/%s/%s", subdir, value);
	return realpath(buf, NULL);
}
#endif

status_t wdcset_skip_devices(const char *value, struct wbargs *wb)
{
	size_t n;
	status_t ret = STATUS_CARRYON;
	char *devlist;
	char *item = NULL;

	if (!(wb->f && wb->f->devpath))
		return STATUS_CARRYON;
	devlist = strtoargz(value, ',', &n);
	while ((item = argz_get(devlist, n, item)) && ret == STATUS_CARRYON) {
#ifdef __linux__
		char *real_dev = get_real_dev(item);
		if (!strcmp(real_dev ? real_dev : item, wb->f->devpath))
			ret = STATUS_DISABLE;
		free(real_dev);
#else
		if (!strcmp(item, wb->f->devpath))
			ret = STATUS_DISABLE;
#endif
	}
	free(devlist);
	return ret;
}

status_t wdcset_timeout(const char *value, struct wbargs *wb)
{
	if ((wb->timeout = strtime(value)) < 0) {
		error(0, 0, "%s: input \"%s\" is invalid, setting default timeout", __func__, value);
		wb->timeout = WD_TIMEOUT_DEFAULT;
		return STATUS_CARRYON;
	}
	if (wb->timeout > 0 && wb->timeout < WD_TIMEOUT_MIN) {
		error(0, 0, "%s: timeout %d below minimum (%ds), disabling cache",
		      __func__, wb->timeout, WD_TIMEOUT_MIN);
		wb->timeout = 0;
	}
	return STATUS_CARRYON;
}

status_t wdcset_blocksize(const char *value, struct wbargs *wb)
{
	wb->blocksize = get_block_size(value, NULL);
	return STATUS_CARRYON;
}

void read_env(struct wbargs *wb)
{
	int i;
	status_t ret = STATUS_CARRYON;
	char *envvar;

	if (wb->flags & WDSIZE_NOENV)
		return;
	for (i = 0; wdconfig[i].key && ret == STATUS_CARRYON; i++)
		if (wdconfig[i].env && (envvar = getenv(wdconfig[i].env)) && wdconfig[i].scope & SCOPE_ENV)
			ret = wdconfig[i].parser(envvar, wb);
	if (ret == STATUS_DISABLE)
		wdexit(0, NULL);
}

char *get_fs_value(struct stat *sb, struct fsinfo *f)
{
	(void)sb;
	return strdup(f->fstype);
}

char *get_dev_value(struct stat *sb, struct fsinfo *f)
{
	(void)sb;
	return strdup(f->devpath);
}

char *get_uid_value(struct stat *sb, struct fsinfo *f)
{
	char *buf = malloc(SLMINBUF);

	(void)f;
	snprintf(buf, SLMINBUF - 1, "%d", sb->st_uid);
	return buf;
}

char *get_gid_value(struct stat *sb, struct fsinfo *f)
{
	char *buf = malloc(SLMINBUF);

	(void)f;
	snprintf(buf, SLMINBUF - 1, "%d", sb->st_gid);
	return buf;
}

char *get_user_value(struct stat *sb, struct fsinfo *f)
{
	struct passwd *pwb;

	(void)f;
	pwb = getpwuid(sb->st_uid);
	return pwb ? strdup(pwb->pw_name) : NULL;
}

char *get_group_value(struct stat *sb, struct fsinfo *f)
{
	struct group *gb;

	(void)f;
	gb = getgrgid(sb->st_gid);
	return gb ? strdup(gb->gr_name) : NULL;
}

char *get_term_value(struct stat *sb, struct fsinfo *f)
{
	(void)f;
	(void)sb;
	char *term = getenv("TERM");

	if (!term)
		return NULL;
	return strdup(term);
}

status_t read_rule_contents(ini_section sb, struct wbargs *wb, int scope)
{
	int i;
	status_t ret = STATUS_CARRYON;
	struct ini_key *kv = NULL;

	if (!sb)
		return STATUS_CARRYON;
	for (i = 0; wdconfig[i].key && ret != STATUS_DISABLE; i++) {
		if ((kv = ini_key_get(sb, wdconfig[i].key)) && wdconfig[i].scope & scope)
			ret = wdconfig[i].parser(kv->value, wb);
	}
	if (ret == STATUS_DISABLE)
		wdexit(0, NULL);
	return ret;
}

void get_color_scheme(inistream ib, const char *scheme, struct colorset **cb)
{
	char query[SLMINBUF] = "";
	ini_section sb_color;
	struct ini_key *kv = NULL, *term_generic = NULL;
	struct colorset *new = NULL;

	if (isempty(scheme))
		return;
	snprintf(query, sizeof(query) - 1, "$color:%s", scheme);
	if (!(sb_color = ini_section_get(ib, query, INI_STRCMP)))
		return;
	/* allowing multiple comma-separated terminal names means
	 * it has to sift through all names in search of the current
	 * terminal */
	while ((kv = ini_key_get_next(sb_color, NULL, kv)) && !new) {
		/* no colors? */
		if (isempty(kv->value) && is_term(kv->name, NULL))
			return;
		if (is_term(kv->name, NULL))
			new = get_colorset(kv->value);
		if (is_term(kv->name, "?") && !isempty(kv->value))
			term_generic = kv;
	}
	if (!new && term_generic)
		new = get_colorset(term_generic->value);
	cbfree(cb);
	*cb = new;
}

inistream read_rule(const char *filename, struct wbargs *wb)
{
	struct stat stb;
	inistream ib;
	ini_section sb_dir, sb_dot, sb_tag;
	char query[SLMINBUF] = "";
	char *tagv;
	int i;

	if (!filename)
		return NULL;
	if (!(ib = ini_open(filename, INI_TRY_ALL))) {
		fprintf(stderr, "%s: %s: %s", __func__, filename, strerror(errno));
		return NULL;
	}
	sb_dot = ini_read(ib, ".", 0);
	sb_dir = ini_read(ib, wb->dirpath, 0);
	if (stat(wb->dirpath, &stb) < 0)
		raise(SIGUSR1);
	read_rule_contents(sb_dot, wb, SCOPE_DOT);
	for (i = 0; tag[i].name; i++) {
		if (tag[i].get_tag_value && (tagv = tag[i].get_tag_value(&stb, wb->f)))
			snprintf(query, sizeof(query) - 1, "^\\$%s:%s$", tag[i].name, tagv);
		else
			snprintf(query, sizeof(query) - 1, "\\$%s:.*", tag[i].name);
		if ((sb_tag = ini_read(ib, query, INI_FORCE_REGEX)) && tagv)
			read_rule_contents(sb_tag, wb, SCOPE_FS);
		free(tagv);
		tagv = NULL;
	}
	read_rule_contents(sb_dir, wb, SCOPE_DIR);
	get_color_scheme(ib, wb->color_scheme, &wb->cb);
	return ib;
}

void wbfree(struct wbargs **wb)
{
	if (!*wb)
		return;
	if ((*wb)->fmt)
		free((*wb)->fmt);
	cbfree(&(*wb)->cb);
	fifree(&(*wb)->f);
	free((*wb)->color_scheme);
	if ((*wb)->xtconf.subdirs)
		free((*wb)->xtconf.subdirs);
	free((*wb)->conf_user);
	free(*wb);
	*wb = NULL;
}

void version(void)
{
	fprintf(stderr, "%s %s\n", PACKAGE, VERSION);
	exit(EXIT_FAILURE);
}

void help(void)
{
	fprintf(stderr, "%s <options>\n\n"
		"-r <bool>	recursive\n"
		"-x <bool>	xdev\n"
		"-E <bool>	read environment variables\n"
		"-f <string>	output format\n"
		"-F <string>	skip filesystems\n"
		"-d <string>	skip devices\n"
		"-o <string>	skip fs options\n"
		"-c [string]	color. an empty argument disables color\n"
		"-t <time>	cache timeout (30s, 5m, 10m3s, 4h etc.)\n"
		"-b <string>	block size (1024, 1K, 1M, 2G etc.)\n",
		PACKAGE);
	exit(EXIT_FAILURE);
}

int get_fsinfo_field(char *dest, size_t n, uint64_t blocksize, const struct fsinfo *f, int field)
{
	if (!f)
		return 0;
	switch (field) {
	case 'C':
		strfbase(dest, n, blocksize, f->b_free);
		break;
	case 'e':
		strfbase(dest, n, blocksize, f->b_used);
		break;
	case 'm':
		if (f->mountpoint)
			strncpy(dest, f->mountpoint, n);
		break;
	case 'p':
		snprintf(dest, n, "%.1f", f->free_percent);
		break;
	case 'P':
		snprintf(dest, n, "%.1f", f->used_percent);
		break;
	case 'S':
		strfbase(dest, n, blocksize, f->b_total);
		break;
	case 't':
		if (f->fstype)
			strncpy(dest, f->fstype, n);
		break;
	}
	return *dest;
}

/* %a: total file count
 * %A: total file count excluding directories
 * %b: apparent size
 * %B: disk usage
 * %c: number of character devices
 * %C: fs free bytes (blocksizeable)
 * %d: number of directories
 * %D: current directory
 * %e: fs used space
 * %f: number of FIFO files
 * %k: number of block devices
 * %l: number of symlinks
 * %L: number of hardlinks
 * %m: mountpoint
 * %o: disk usage, fall back to apparent usage if xt_blocks == 0
 * %p: percentage of free fs bytes
 * %P: percentage of used fs bytes
 * %r: number of regular files
 * %s: number of sockets
 * %S: fs size
 * %t: fs type
 * %z: block size
 */
int get_wdout_field(char *dest, size_t n, const void *buf, int field)
{
	const struct fmtinfo *fb = buf;
	const uint64_t *nfiles = NULL;

	switch (field) {
	case 'B':
		strfbase(dest, n, fb->blocksize, fb->xb->xt_blocks * 512);
		break;
	case 'o':
		if (fb->xb->xt_blocks) {
			strfbase(dest, n, fb->blocksize, fb->xb->xt_blocks * 512);
			break;
		}
	case 'b':
		strfbase(dest, n, fb->blocksize, fb->xb->xt_size);
		break;
	case 'r':
		nfiles = &fb->xb->xt_nreg;
		break;
	case 'd':
		nfiles = &fb->xb->xt_ndir;
		break;
	case 'c':
		nfiles = &fb->xb->xt_nchr;
		break;
	case 'k':
		nfiles = &fb->xb->xt_nblk;
		break;
	case 'f':
		nfiles = &fb->xb->xt_nfifo;
		break;
	case 'l':
		nfiles = &fb->xb->xt_nlnk;
		break;
	case 's':
		nfiles = &fb->xb->xt_nsock;
		break;
	case 'L':
		nfiles = &fb->xb->xt_nhlnk;
		break;
	case 'a':
		nfiles = &fb->xb->xt_ntotal;
		break;
	case 'A':
		nfiles = &fb->xb->xt_ndtotal;
		break;
	case 'z':
		strfbase(dest, n, fb->blocksize % IEC_BASE == 0 ? IEC_BASE : SI_BASE, fb->blocksize);
		break;
	case 'D':
		strncpy(dest, fb->dirpath, n);
		break;
	default:
		return get_fsinfo_field(dest, n, fb->blocksize, fb->f, field);
	}
	if (nfiles)
		snprintf(dest, n, "%'lu", *nfiles);
	return *dest;
}

void wdexit(int exitcode, const char *msg)
{
	printf("export wdcolor='';\n");
	printf("export wdout='%s';\n", msg ? msg : "");
	exit(exitcode);
}

void sighandler(int signal)
{
	const char *msg = NULL;

	switch (signal) {
	case SIGSEGV:
		msg = "SEGV";
		break;
	case SIGABRT:
		msg = "ABRT";
		break;
	case SIGTERM:
		msg = "TERM";
		break;
	case SIGUSR1:
		msg = "ERR";
		break;
	}
	wdexit(EXIT_FAILURE, msg);
}

uint64_t string_checksum(const char *s)
{
	uint64_t sum = 5381;
	int p = 1;

	while ((p = *s++))
		sum = ((sum << 5) + sum) + p;
	return sum;
}

uint64_t get_checksum(const struct xstat *const xb, const char *const dirpath, time_t tv_sec)
{
	uint64_t xbsum, sbsum;

	sbsum = (xb->sb.st_dev ^ xb->sb.st_ino)
		 + xb->sb.st_mode
		 + xb->sb.st_nlink
		 + xb->sb.st_uid
		 + xb->sb.st_gid
		 + xb->sb.st_blocks
		 + xb->sb.st_size
		 + xb->sb.st_atim.tv_sec
		 + xb->sb.st_atim.tv_nsec
		 + xb->sb.st_mtim.tv_sec
		 + xb->sb.st_mtim.tv_nsec;
	xbsum = string_checksum(dirpath)
		 + xb->flags
		 + xb->xt_size
		 + xb->xt_blocks
		 + xb->xt_nreg
		 + xb->xt_ndir
		 + xb->xt_nchr
		 + xb->xt_nblk
		 + xb->xt_nfifo
		 + xb->xt_nlnk
		 + xb->xt_nsock
		 + xb->xt_nhlnk
		 + xb->xt_ntotal
		 + xb->xt_ndtotal;
	return ~((xbsum + sbsum + tv_sec) << 3);
}

inistream cache_open(char **inodev, unsigned long dev, unsigned long ino)
{
	char tmpname[SLBUFSIZ];
	static char buf[SLBUFSIZ];
	inistream ret;

	memset(buf, 0, sizeof(buf));
	snprintf(tmpname, sizeof(tmpname), "/tmp/%s-%d", PACKAGE, getuid());
	snprintf(buf, sizeof(buf) - 1, "%lu:%lu", ino, dev);
	if (!(ret = ini_open(tmpname, INI_WRITE|INI_CREAT|INI_STRCMP|INI_WRLOCK))) {
		error(0, errno, "%s: ini_open", __func__);
		return NULL;
	}
	if (chmod(tmpname, S_IRUSR|S_IWUSR) < 0) {
		error(0, errno, "%s: chmod", __func__);
		ini_close(&ret);
		return NULL;
	}
	*inodev = buf;
	return ret;
}

void db_add_to_cache(const struct xstat *const db, const char *const dirpath)
{
	inistream ib;
	ini_section sb;
	struct timespec tm;
	char *inodev;

	clock_gettime(CLOCK_SOURCE, &tm);
	if (!(ib = cache_open(&inodev, db->sb.st_dev, db->sb.st_ino)))
		return;
	sb = ini_section_new(ib, inodev);
	ini_key_newf(sb, "checksum", "%#lx", get_checksum(db, dirpath, tm.tv_sec));
	ini_key_newf(sb, "tm_sec", "%ld", tm.tv_sec);
	ini_key_newf(sb, "bytes", "%ld", db->xt_size);
	ini_key_newf(sb, "blocks", "%ld", db->xt_blocks);
	ini_key_newf(sb, "nreg", "%ld", db->xt_nreg);
	ini_key_newf(sb, "ndir", "%ld", db->xt_ndir);
	ini_key_newf(sb, "nchr", "%ld", db->xt_nchr);
	ini_key_newf(sb, "nblk", "%ld", db->xt_nblk);
	ini_key_newf(sb, "nfifo", "%ld", db->xt_nfifo);
	ini_key_newf(sb, "nlnk", "%ld", db->xt_nlnk);
	ini_key_newf(sb, "nsock", "%ld", db->xt_nsock);
	ini_key_newf(sb, "nhlnk", "%ld", db->xt_nhlnk);
	ini_key_newf(sb, "ntotal", "%ld", db->xt_ntotal);
	ini_key_newf(sb, "nodirtotal", "%ld", db->xt_ndtotal);
	ini_key_newf(sb, "flags", "%d", db->flags);
	if (ini_write_to_stream(ib, sb) < 0)
		error(0, errno, "%s: ini_write", __func__);
	ini_close(&ib);
}

struct xstat *db_get_from_cache(struct wbargs *wb)
{
	inistream ib;
	ini_section is;
	struct stat sb;
	time_t tm_then = 0;
	struct timespec tm;
	struct ini_key *kv = NULL;
	uint64_t dbsum = 0, rt_dbsum;
	char *inodev;
	struct xstat *xb;

	if (wb->timeout < WD_TIMEOUT_MIN)
		return NULL;
	/* now */
	clock_gettime(CLOCK_SOURCE, &tm);
	if (stat(wb->dirpath, &sb) < 0)
		return NULL;
	if (!(ib = cache_open(&inodev, sb.st_dev, sb.st_ino)))
		return NULL;
	/* then */
	if (!(is = ini_read(ib, inodev, 0))) {
		ini_close(&ib);
		return NULL;
	}
	kv = NULL;
	xb = malloc(sizeof(*xb));
	memset(xb, 0, sizeof(*xb));
	while ((kv = ini_key_get_next(is, NULL, kv))) {
		if (!strcmp(kv->name, "tm_sec"))
			tm_then = atoll(kv->value);
		else if (!strcmp(kv->name, "flags"))
			xb->flags = atoll(kv->value);
		else if (!strcmp(kv->name, "bytes"))
			xb->xt_size = atoll(kv->value);
		else if (!strcmp(kv->name, "blocks"))
			xb->xt_blocks = atoll(kv->value);
		else if (!strcmp(kv->name, "nreg"))
			xb->xt_nreg = atoll(kv->value);
		else if (!strcmp(kv->name, "nreg"))
			xb->xt_nreg = atoll(kv->value);
		else if (!strcmp(kv->name, "ndir"))
			xb->xt_ndir = atoll(kv->value);
		else if (!strcmp(kv->name, "nchr"))
			xb->xt_nchr = atoll(kv->value);
		else if (!strcmp(kv->name, "nblk"))
			xb->xt_nblk = atoll(kv->value);
		else if (!strcmp(kv->name, "nfifo"))
			xb->xt_nfifo = atoll(kv->value);
		else if (!strcmp(kv->name, "nlnk"))
			xb->xt_nlnk = atoll(kv->value);
		else if (!strcmp(kv->name, "nsock"))
			xb->xt_nsock = atoll(kv->value);
		else if (!strcmp(kv->name, "nhlnk"))
			xb->xt_nhlnk = atoll(kv->value);
		else if (!strcmp(kv->name, "ntotal"))
			xb->xt_ntotal = atoll(kv->value);
		else if (!strcmp(kv->name, "nodirtotal"))
			xb->xt_ndtotal = atoll(kv->value);
		else if (!strcmp(kv->name, "checksum"))
			dbsum = strtoull(kv->value, NULL, 0);
	}
	ini_close(&ib);
	xb->sb = sb;
	rt_dbsum = get_checksum(xb, wb->dirpath, tm_then);
	if (rt_dbsum == dbsum && tm.tv_sec - tm_then < wb->timeout)
		return xb;
	free(xb);
	return NULL;
}

struct wbargs *wballoc(void)
{
	struct wbargs *wb;
	char *conf_user;

	if (!(wb = calloc(1, sizeof(*wb)))) {
		error(0, errno, "%s", __func__);
		raise(SIGUSR1);
	}
	if (!getcwd(wb->dirpath, sizeof(wb->dirpath) - 1)) {
		error(0, errno, "%s: getcwd", __func__);
		raise(SIGUSR1);
	}
	wb->xtconf.flags = XSTAT_XDEV|XSTAT_CHDIR;
	wb->xtconf.dirpath = wb->dirpath;
	wb->fmt = strdup(WDOUT_FMTDEFAULT);
	wb->timeout = WD_TIMEOUT_DEFAULT;
	if (!(wb->f = get_fsinfo(wb->dirpath)))
		wdexit(1, "FSERR");
	if (access(WDCONF_SYSTEM, F_OK|R_OK) == 0)
		wb->conf_system = WDCONF_SYSTEM;
	if (access(WDCONF_CWD, F_OK|R_OK) == 0)
		wb->conf_cwd = WDCONF_CWD;
	conf_user = expand_tilde(WDCONF_USER);
	if (access(conf_user, F_OK|R_OK) == 0)
		wb->conf_user = conf_user;
	return wb;
}

int main(int argc, char **argv)
{
	char *fmt;
	struct wbargs *wb;
	status_t a = STATUS_CARRYON;
	int c;
	pthread_t thr;
	const int siglist[] = { SIGSEGV, SIGABRT, SIGTERM, SIGUSR1, 0 };
	struct sigaction act = { .sa_handler = sighandler };
	struct xstat *xb;
	struct fmtinfo fmtbuf;
	struct progress_caps pr = { NULL, NULL };
	inistream conf_system, conf_user, conf_cwd;

	setlocale(LC_ALL, "");
	for (c = 0; siglist[c]; c++) {
		if (sigaction(siglist[c], &act, NULL) < 0) {
			error(0, errno, "sigaction");
			wdexit(EXIT_FAILURE, NULL);
		}
	}
	wb = wballoc();
	setupcolor(STDOUT_FILENO);
	conf_system = read_rule(wb->conf_system, wb);
	conf_user = read_rule(wb->conf_user, wb);
	conf_cwd = read_rule(wb->conf_cwd, wb);
	while ((c = getopt(argc, argv, "+hvr:x:f:F:d:o:c::t:b:E:")) != -1 && a == STATUS_CARRYON) {
		switch (c) {
		case 'h':
			help();
		case 'v':
			version();
		case 'r':
			a = wdcset_recursive(optarg, wb);
			break;
		case 'x':
			a = wdcset_xdev(optarg, wb);
			break;
		case 'f':
			a = wdcset_format(optarg, wb);
			break;
		case 'F':
			a = wdcset_skip_fs(optarg, wb);
			break;
		case 'd':
			a = wdcset_skip_devices(optarg, wb);
			break;
		case 'o':
			a = wdcset_skip_fs_opts(optarg, wb);
			break;
		case 'c':
			a = wdcset_color(optarg ? optarg : "", wb);
			if (optarg) {
				if (conf_system)
					get_color_scheme(conf_system, wb->color_scheme, &wb->cb);
				if (conf_user)
					get_color_scheme(conf_user, wb->color_scheme, &wb->cb);
				if (conf_cwd)
					get_color_scheme(conf_cwd, wb->color_scheme, &wb->cb);
			}
			break;
		case 't':
			a = wdcset_timeout(optarg, wb);
			break;
		case 'b':
			a = wdcset_blocksize(optarg, wb);
			break;
		case 'E':
			a = wdcset_noenv(optarg, wb);
			break;
		default:
			exit(EXIT_FAILURE);
		}
	}
	ini_close(&conf_system);
	ini_close(&conf_user);
	ini_close(&conf_cwd);
	if (a == STATUS_DISABLE) {
		wbfree(&wb);
		wdexit(EXIT_SUCCESS, NULL);
	}
	read_env(wb);
	if (wb->color_scheme && !wb->cb && !(wb->cb = get_colorset(wb->color_scheme))) {
		fprintf(stderr, "%s: color scheme \"%s\" not found or invalid color string\n", PACKAGE, wb->color_scheme);
		fprintf(stderr, "%s: if you want no color, set value as empty\n", PACKAGE);
		fprintf(stderr, "%s: alternatively, your terminal has no color support\n", PACKAGE);
	}
	if ((pr.cr = tigetstr("cr")) == (char *)-1) {
		set_flag(wb->flags, 1, WDSIZE_NOPROGRESS);
		pr.cr = NULL;
	}
	if ((pr.el = tigetstr("el")) == (char *) -1) {
		set_flag(wb->flags, 1, WDSIZE_NOPROGRESS);
		pr.el = NULL;
	}
	if (!(xb = db_get_from_cache(wb))) {
		if (!(wb->flags & WDSIZE_NOPROGRESS)) {
			if (pthread_create(&thr, NULL, progress_stalled, &pr) == 0)
				pthread_detach(thr);
		}
		if (!(xb = xstat(&wb->xtconf)))
			raise(SIGUSR1);
		if (wb->timeout)
			db_add_to_cache(xb, wb->dirpath);
	}
	if (wb->cb) {
		printf("export wdcolor='%s%s%s';\n",
			wb->cb->attr ? wb->cb->attr : "",
			wb->cb->fg ? wb->cb->fg : "",
			wb->cb->bg ? wb->cb->bg : "");
		printf("export wdcreset='%s';\n", wb->cb->reset);
	} else {
		printf("export wdcolor='';\n"
			"export wdcreset='';\n");
	}
	fmtbuf.dirpath = wb->dirpath;
	fmtbuf.blocksize = wb->blocksize;
	fmtbuf.xb = xb;
	fmtbuf.f = wb->f;
	if (!(fmt = strformat(wb->fmt, &fmtbuf, get_wdout_field)))
		wdexit(EXIT_FAILURE, "FMT");
	printf("export wdout='%s';\n", fmt);
	wbfree(&wb);
	free(xb);
	free(fmt);
	if (cur_term)
		del_curterm(cur_term);
	return EXIT_SUCCESS;
}


void *progress_stalled(void *p)
{
	char c = 0;
	struct progress_caps *pr = p;

	sleep(5);
	if (!pr->cr || !pr->el) {
		fprintf(stderr, "wdsize is working...\n");
		return NULL;
	}
	while (1) {
		c = (c == '\\') ? '|' : (c == '|') ? '/' : (c == '/') ? '-' : '\\';
		fprintf(stderr, "%s%s[%c] wdsize is working...", pr->el, pr->cr, c);
		usleep(1000);
	}
	return p;
}
