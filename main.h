/*
 * main.h
 *
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#ifndef WDSIZE_H
#define WDSIZE_H

#include "strlib.h"
#include "xstat.h"
#include "color.h"

#define WDOUT_FMTDEFAULT	"%B/%a"

#ifndef SYSCONFDIR
# define SYSCONFDIR "/usr/local/etc"
#endif

#define WDCONF_USER		"~/.wdsizerc"
#define WDCONF_SYSTEM		SYSCONFDIR"/wdsize/wdsize.conf"
#define WDCONF_CWD		"./.wdsize"
#define WD_TIMEOUT_DEFAULT	60
#define WD_TIMEOUT_MIN		30

#define WDSIZE_NOENV		(1 << 1)
#define WDSIZE_NOPROGRESS	(1 << 2)

#define set_flag(v, i, flag)	(i == 0 ? v &= ~flag : i == 1 ? v |= flag : 0)

struct wbargs {
	int flags;
	const char *conf_system;
	char *conf_user;
	const char *conf_cwd;
	char dirpath[SLBUFSIZ];
	char *fmt;
	char *color_scheme;
	struct colorset *cb;
	time_t timeout;
	uint64_t blocksize;
	struct fsinfo *f;
	struct xtconfig xtconf;
};

typedef enum {
	STATUS_NOCONFIG,
	STATUS_CARRYON,
	STATUS_DISABLE
} status_t;

#define SCOPE_FS		(1 << 0)
#define SCOPE_DIR		(1 << 1)
#define SCOPE_ENV		(1 << 2)
#define SCOPE_CMDLINE		(1 << 3)
#define SCOPE_DOT		(1 << 4)
#define SCOPE_GLOBAL		(SCOPE_CMDLINE|SCOPE_DOT|SCOPE_ENV)
#define SCOPE_LOCAL		(SCOPE_FS|SCOPE_DIR|SCOPE_ENV)
#define SCOPE_ALL		(SCOPE_FS|SCOPE_DIR|SCOPE_ENV|SCOPE_CMDLINE|SCOPE_DOT)

#define TAG_LABEL		"LABEL"
#define TAG_PARTLABEL		"PARTLABEL"
#define TAG_PARTUUID		"PARTUUID"
#define TAG_UUID		"UUID"
#define TAG_ID			"ID"
#define TAG_LABEL_DIR		"by-label"
#define TAG_UUID_DIR		"by-uuid"
#define TAG_PARTUUID_DIR	"by-partuuid"
#define TAG_PARTLABEL_DIR	"by-partlabel"
#define TAG_ID_DIR		"by-id"

void wbfree(struct wbargs **wb);
struct wbargs *wballoc(void);
void *progress_stalled(void *p);
status_t wdcset_enabled(const char *value, struct wbargs *wb);
status_t wdcset_recursive(const char *value, struct wbargs *wb);
status_t wdcset_xdev(const char *value, struct wbargs *wb);
status_t wdcset_color(const char *value, struct wbargs *wb);
status_t wdcset_format(const char *value, struct wbargs *wb);
status_t wdcset_skip_fs(const char *value, struct wbargs *wb);
status_t wdcset_skip_fs_opts(const char *value, struct wbargs *wb);
status_t wdcset_skip_devices(const char *value, struct wbargs *wb);
status_t wdcset_timeout(const char *value, struct wbargs *wb);
status_t wdcset_blocksize(const char *value, struct wbargs *wb);
status_t wdcset_noenv(const char *value, struct wbargs *wb);
status_t wdcset_noprogress(const char *value, struct wbargs *wb);
status_t wdcset_subdirs(const char *value, struct wbargs *wb);
char *get_fs_value(struct stat *sb, struct fsinfo *f);
char *get_dev_value(struct stat *sb, struct fsinfo *f);
char *get_uid_value(struct stat *sb, struct fsinfo *f);
char *get_gid_value(struct stat *sb, struct fsinfo *f);
char *get_user_value(struct stat *sb, struct fsinfo *f);
char *get_group_value(struct stat *sb, struct fsinfo *f);
char *get_term_value(struct stat *sb, struct fsinfo *f);
void wdexit(int exitcode, const char *msg);

#endif
