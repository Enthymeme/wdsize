/*
 * Copyright (C) Ludvig Hummel
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
 */

#include "strlib.h"
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <locale.h>
#include <langinfo.h>
#include <curses.h>
#include <term.h>
#include <pwd.h>

int strbool(const char *s)
{
	int i;
	const char *strfalse[] = {
		"false", "0", "no", "n", "disable", "disabled", "off",
		NULL };
	const char *strtrue[] = {
		"true", "1", "yes", "y", "enable", "enabled", "on",
		NULL };

	for (i = 0; strfalse[i]; ++i) {
		if (!strcmp(s, strfalse[i]))
			return 0;
	}

	for (i = 0; strtrue[i]; ++i) {
		if (!strcmp(s, strtrue[i]))
			return 1;
	}
	return -1;
}

void stolower(char *s)
{
	while (*s) {
		*s = tolower(*s);
		s++;
	}
}

void stoupper(char *s)
{
	while (*s) {
		*s = toupper(*s);
		s++;
	}
}

char *strncatf(char *dest, size_t n, const char *fmt, ...)
{
	size_t len;
	va_list ap;

	len = strlen(dest);
	dest += len;
	n -= len;

	va_start(ap, fmt);
	vsnprintf(dest, n, fmt, ap);
	va_end(ap);

	return dest;
}

int rmtrailzero(int c)
{
	return c == '0';
}

char *strfbase(char *buf, size_t n, int base, double blkcnt)
{
	const char *prefix = "bKMGTPEZ";
	size_t len;

	if (!base)
		base = IEC_BASE;
	for (; blkcnt >= base && *prefix; prefix++)
		blkcnt /= base;
	snprintf(buf, n, "%'.2F", blkcnt);
	strstrip(buf, rmtrailzero);
	len = strlen(buf) - 1;
	if (strcmp(&buf[len], nl_langinfo(RADIXCHAR)) != 0)
		len++;
	buf[len] = 0;
	if (!(base == IEC_BASE || base == SI_BASE))
		return buf;
	if (*prefix == 'b') {
		strncat(buf, " bytes", bufcatlen(buf, n));
		return buf;
	}
	if (base == SI_BASE) {
		buf[len] = *prefix;
		strncat(buf, "B", bufcatlen(buf, n));
	} else if (base == IEC_BASE) {
		buf[len] = *prefix;
		strncat(buf, "iB", bufcatlen(buf, n));
	}
	return buf;
}

uint64_t get_block_size(const char *str, char **tail)
{
	char *endptr;
	short base = BASE_TWO;
	uint64_t result = 0;
	int *exponent = NULL;
	int i;
	struct {
		const char *unit;
		int exponent;
	} units[] = {
		{"kB", 3 },
		{"MB", 6 },
		{"GB", 9 },
		{"TB", 12 },
		{"PB", 15 },
		{"EB", 18 },
		{"KiB", 10 },
		{"MiB", 20 },
		{"GiB", 30 },
		{"TiB", 40 },
		{"PiB", 50 },
		{"EiB", 60 },
		{ NULL,  0 }
	};

	result = strtoull(str, &endptr, 10);
	if (str == endptr)
		return 0;
	if (!endptr || !*endptr)
		return result;
	for (i = 0; units[i].unit && !exponent; i++) {
		if (!strncmp(endptr, units[i].unit, strlen(endptr))) {
			exponent = &units[i].exponent;
			base = units[i].unit[1] == 'i' ? BASE_TWO : BASE_TEN;
			if (tail)
				*tail = endptr + strlen(units[i].unit);
		}
	}
	if (!exponent)
		return result;
	for (i = 0; i < *exponent; i++)
		result *= base;
	return result;
}

time_t strtime(const char *s)
{
	char *endptr;
	time_t ret = 0, calc = 0;

	calc = strtol(s, &endptr, 10);
	while (*endptr) {
		switch (toupper(*endptr)) {
		case 'H':
			calc *= 60 * 60;
			break;
		case 'M':
			calc *= 60;
			break;
		case 'S':
			break;
		default:
			return -1;
		}
		ret += calc;
		while (*endptr && isalpha(*endptr))
			endptr++;
		if (!*endptr)
			break;
		calc = strtol(endptr, &endptr, 10);
	}
	return (ret == 0 ? calc : ret);
}

char *strnqchr(const char *s, int c)
{
	int quoted = 0;

	/* The first backslash will always be unquoted */
	if (!strchr(s, '\\') || (c == '\\'))
		return strchr(s, c);
	do {
		if (*s == c && !quoted)
			return (char *)s;
		quoted = !quoted && *s == '\\';
	} while (*s++);
	return NULL;
}

char **argztoargv(char *argz, size_t size)
{
	int i;
	char **ret = NULL, **newp;
	char *p = NULL;

	for (i = 0; (p = argz_get(argz, size, p)) && i < size; i++) {
		if (!(newp = realloc(ret, sizeof(*ret) * (i + 2)))) {
			free(ret);
			return NULL;
		}
		ret = newp;
		ret[i] = p;
	}
	ret[i] = NULL;
	return ret;
}

char *argz_get(char *argz, size_t size, char *entry)
{
	size_t i = 0;

	if (!entry)
		return argz;
	i = entry - argz;
	argz = entry;
	while (i < size && *entry) {
		entry++;
		i++;
	}
	while (i < size && *entry == 0) {
		entry++;
		i++;
	}
	return i >= size ? NULL : entry;
}

char *strtoargz(const char *src, int sep, size_t *size)
{
	char *ret, *p;
	const char *quote = NULL;
	size_t len = strlen(src);
	int i;

	*size = len + 1;
	if (!(ret = calloc(*size, sizeof(*ret))))
		return NULL;
	while (*src == sep)
		src++;
	p = ret;
	len = 0;
	for (i = 0; *src && i < *size; src++, i++) {
		if ((*src == '\\' || *src == '"') && !quote) {
			quote = src;
		} else if (quote) {
			if (*quote == '"' && *src == '"' && src > quote)
				quote = 0;
			else if (*quote == '\\')
				quote = 0;
		}
		if (*src == sep && !quote) {
			strtrim(p);
			len += strlen(p) + 1;
			ret[i = len] = 0;
			p = &ret[i];
			while (*src && (*src == sep || isblank(*src) || isspace(*src)))
				src++;
			if (!*src)
				break;
			if (*src == '"' || *src == '\\')
				quote = src;
		}
		ret[i] = *src;
	}
	strtrim(p);
	return ret;
}

char *get_home_dir(void)
{
	struct passwd *pw = getpwuid(getuid());
	char *homedir = getenv("HOME");

	return homedir && !isempty(homedir) ? homedir : pw ? pw->pw_dir : ".";
}

int is_regex(const char *pattern)
{
	regex_t rx;
	int re = regcomp(&rx, pattern, REG_EXTENDED|REG_NOSUB);
	char buf[SLBUFSIZ];

	if (re) {
		regerror(re, &rx, buf, sizeof(buf));
		fprintf(stderr, "%s: %s: %s\n", __func__, pattern, buf);
	}
	regfree(&rx);
	return !re;
}

int regmatch(const char *buf, const char *pattern, int cflags)
{
	regex_t rx;
	int re = regcomp(&rx, pattern, cflags|REG_NOSUB);
	char rebuf[SLBUFSIZ];
	int ret = -1;

	if (re) {
		regerror(re, &rx, rebuf, sizeof(rebuf));
		fprintf(stderr, "%s: %s: %s\n", __func__, pattern, rebuf);
		return ret;
	}
	ret = regexec(&rx, buf, 0, NULL, 0);
	regfree(&rx);
	return ret;
}

static int strlib_fallback_filter(int c)
{
	return iscntrl(c) || isblank(c) || c == '\n';
}

char *strstrip(char *s, int (*filter)(int c))
{
	char *p = s;

	if (!*s)
		return s;
	if (!filter)
		filter = strlib_fallback_filter;
	while (*p && filter(*p))
		p++;
	strbtrim(p, filter);
	return s;
}

char *strbtrim(char *s, int (*filter)(int c))
{
	char *p, *begin = s;
	size_t len, s_len = strlen(s);

	if (!filter)
		filter = strlib_fallback_filter;
	if (!*s)
		return s;
	p = s;
	while (*p && filter(*p))
		p++;
	len = strlen(p);
	memmove(s, p, len);
	memset(&s[len], 0, s_len - len);
	p = strchr(s, 0) - 1;
	while (p >= begin && filter(*p)) {
		*p = 0;
		p--;
	}
	return begin;
}

char *strtidy(char *s, int (*newchar)(int oldchar))
{
	char *start;
	int c;

	for (start = s; *s; s++) {
		if ((c = newchar(*s)) < 0)
			continue;
		if (c)
			*s = c;
		if (s > start && (!c || *(s - 1) == c)) {
			memmove(s, s + 1, strlen(s - 2));
			s--;
		}
	}
	return start;
}

regmatch_t *ismatch(regoff_t rm_so, regmatch_t pmatch[], size_t nmatch)
{
	while (nmatch--)
		if (pmatch[nmatch].rm_so == rm_so)
			return &pmatch[nmatch];
	return NULL;
}

static int is_quoted(const char *buf, const regmatch_t *match)
{
	regoff_t i;
	int quoted = 0;
	int c;

	if (match->rm_so == 0)
		return 0;
	c = buf[match->rm_so];
	for (i = match->rm_so - 1; i >= 0 && buf[i] == c; i--)
		quoted = !quoted && buf[i] == c;
	return quoted;
}

char *strformat(const char *fmt, const void *fbuf,
	int (*get_field)(char *, size_t, const void *, int))
{
	regmatch_t mb[100], *match;
	regex_t rb;
	size_t nmatch, i;
	char *ret, *p;
	char optexp[FMTBUFSIZ];
	int opt;

	regcomp(&rb, "%[[:alpha:]]", REG_EXTENDED);
	if ((nmatch = regexecv(&rb, fmt, vsizeof(mb), mb, 0)) == 0) {
		regfree(&rb);
		return strdup(fmt);
	}
	regfree(&rb);
	if (!(ret = calloc(SLBUFSIZ, sizeof(*ret))))
		return NULL;
	p = ret;
	for (i = 0; fmt[i]; p++, i++) {
		memset(optexp, 0, sizeof(optexp));
		if ((match = ismatch(i, mb, vsizeof(mb)))) {
			if (is_quoted(fmt, match)) {
				i++;
				goto skip;
			}
			opt = fmt[match->rm_eo - 1];
			if (get_field(optexp, sizeof(optexp) - 1, fbuf, opt)) {
				strncpy(p, optexp, SLBUFSIZ - strlen(ret) - 1);
				p += strlen(optexp);
				i = match->rm_eo;
				if (!fmt[i])
					break;
			}
		}
skip:
		*p = fmt[i];
	}
	return ret;
}

size_t regexecv(const regex_t *rb, const char *buf, size_t nmatch, regmatch_t pmatch[], int eflags)
{
	size_t i = 0;
	regoff_t rm_offset = 0;

	while (regexec(rb, buf, nmatch - i, &pmatch[i], eflags) == 0 && i < nmatch && *buf) {
		buf += pmatch[i].rm_eo;
		pmatch[i].rm_so += rm_offset;
		pmatch[i].rm_eo += rm_offset;
		rm_offset = pmatch[i].rm_eo;
		i++;
	}
	return i;
}

int isempty(const char *s)
{
	while (*s && (iscntrl(*s) || isspace(*s)))
		s++;
	return *s == 0;
}

static int strip_slash(int c)
{
	return c == '/';
}

char *expand_tilde(const char *s)
{
	const char *homedir = get_home_dir();
	char *ret;

	if (*s != '~')
		return strdup(s);
	switch (s[1]) {
	case '/':
		s += 2;
		break;
	case 0:
		s++;
		break;
	default:
		return strdup(s);
	}
	if (!(ret = calloc(SLBUFSIZ, sizeof(*ret))))
		return NULL;
	snprintf(ret, SLBUFSIZ - 1, *s ? "%s/%s" : "%s", homedir, s);
	strstrip(ret, strip_slash);
	return ret;
}
