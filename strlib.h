/*
 * Copyright (C) Ludvig Hummel
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
 */

#ifndef _STRUTIL_H
#define _STRUTIL_H

#if defined __GNUC__ && !defined _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <unistd.h>
#include <stdint.h>
#include <time.h>
#include <regex.h>
#include <ctype.h>

#define ctoi(c) (c - '0')
#define itoc(c) (c + '0')

#define iscomment(s) (s[0] == '#' || s[0] == ';')

#define vsizeof(x) ((sizeof(x) / sizeof (x[0])))

#define SLBUFSIZ 8192
#define SLMINBUF 256

#define boolstr(s) strbool(s) ? "true" : "false"
#define strtrim(s) strbtrim(s, NULL)

#define SI_BASE 1000
#define IEC_BASE 1024

#define BASE_TWO	2
#define BASE_TEN	10

#define FMTBUFSIZ	30
#define OUTBUFSIZ	150

#define bufcatlen(s, n) (n - strlen(s) - 1)

#ifdef __cplusplus
extern "C" {
#endif
char *expand_tilde(const char *s);
int is_regex(const char *pattern);
int strbool(const char *s);
void stoupper(char *s);
void stolower(char *s);
time_t strtime(const char *s);
char *strncatf(char *dest, size_t n, const char *fmt, ...);
uint64_t get_block_size(const char *str, char **tail);
char *strnqchr(const char *s, int c);
char *argz_get(char *argz, size_t size, char *entry);
char *strtoargz(const char *src, int sep, size_t *size);
char **argztoargv(char *argz, size_t size);
int regmatch(const char *buf, const char *pattern, int cflags);
char *strbtrim(char *s, int (*filter)(int c));
char *strstrip(char *s, int (*filter)(int c));
char *strtidy(char *s, int (*newchar)(int oldchar));
char *strfbase(char *buf, size_t n, int base, double blkcnt);
char *get_home_dir(void);
char *strformat(const char *fmt, const void *fbuf,
	int (*get_field)(char *, size_t, const void *, int));
size_t regexecv(const regex_t *rb, const char *buf, size_t nmatch,
	regmatch_t pmatch[], int eflags);
regmatch_t *ismatch(regoff_t rm_so, regmatch_t pmatch[], size_t nmatch);
int isempty(const char *s);


#ifdef __cplusplus
}
#endif

#endif
