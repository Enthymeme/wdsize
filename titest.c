#include <stdio.h>
#include <term.h>
#include <curses.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <err.h>

int main(int argc, char **argv)
{
	char *tistr, *tip, *p;
	char *cap;
	long arg[9];
	int c;

	memset(arg, 0, sizeof(arg));
	while ((c = getopt(argc, argv, "1:2:3:4:5:6:7:8:9:")) != -1) {
		switch (c) {
		/* mumble mumble non-standard mumble mumble */
		case '1'...'9':
			arg[c - '0' - 1] = atoi(optarg);
			break;
		default:
			exit(EXIT_FAILURE);
		}
	}
	if (argc <= optind)
		err(EXIT_FAILURE, "no cap specified");
	cap = argv[optind];
	if (setupterm(NULL, STDOUT_FILENO, NULL) == ERR)
		err(EXIT_FAILURE, "setupterm failed");
	tistr = tigetstr(cap);
	if (tistr && tistr != (char *)-1) {
		if (!(p = strrchr(tistr, '\033')))
			p = tistr;
		else
			p++;
		printf("%s = %s\n", cap, p);
		if ((tip = tiparm(tistr, arg[0], arg[1], arg[2], arg[3], arg[4], arg[5], arg[6], arg[7], arg[8])))
		{
			if (!(p = strrchr(tip, '\033')))
				p = tip;
			else
				p++;
			printf("tparm says = %s\n", p);
			putp(tip);
			printf("test\n");
		}
	} else if (tistr == (char *)-1)
		printf("%s: not a string cap\n", cap);
	else
		printf("%s: cancelled or absent\n", cap);
	if (((tistr = tigetstr("sgr0")) && tistr != (char *)-1) && (tip = tiparm(tistr)))
		putp(tip);
	del_curterm(cur_term);
	return EXIT_SUCCESS;
}
