; the dot rule (or global rule) is read for all dirs
[.]
; show size of the current dir and all of its subdirs (default: false)
; enviroment variable: WDSIZE_RECURSIVE
recursive=0

; don't descend directories on other filesystems (default: true)
; env: WDSIZE_XDEV
xdev=1

; time before rescanning. default is 1m, minimum is 30s
; setting this to 0 disables caching
; env: WDSIZE_TIMEOUT
;timeout=1m

; block size. default (or when it's set to 0) is 'human-readable'
; setting it to 1k (or 1kB) uses SI units instead of binary
; (for those who absolutely need the scam units the hard drive
; manufacturers insist on using)
; env: WDSIZE_BLOCKSIZE
;blocksize=1K

; show progress indicator if the calculation takes too long (usual when
; recursive=1 in a large directory)
; env: WDSIZE_NOPROGRESS
;noprogress=0

; disable/enable wdsize altogether
; env: WDSIZE_ENABLED
;enabled=1

; ignore environment variables (default: 0)
;noenv=0

; %a: total file count
; %A: total file count excluding directories
; %b: apparent size
; %B: size on disk
; %c: number of character devices
; %C: fs free bytes (blocksizeable)
; %d: number of directories
; %D: current directory
; %e: fs used space
; %f: number of FIFO files
; %k: number of block devices
; %l: number of symlinks
; %L: number of hardlinks
; %m: mountpoint
; %o: size on disk (falls back to apparent size if st->st_blocks == 0; primarily meant for jmtpfs)
; %p: percentage of free fs bytes
; %P: percentage of used fs bytes
; %r: number of regular files
; %s: number of sockets
; %S: fs size
; %t: fs type
; %z: block size
;
; %B/%a is the fallback format if one isn't specified
; wdsize makes no effort in searching for or enforcing the presence of
; placeholders, so anything goes
; env: WDSIZE_FORMAT
;format=%B/%a

; skip slow/pseudo-filesystems on Linux (default: empty)
; wildcards are allowed
; env: WDSIZE_SKIP_FS
;skip_fs=fuse*,*tmpfs*,proc,sysfs

; skip filesystems mounted with these options (default: empty)
; env: WDSIZE_SKIP_FS_OPTS
;skip_fs_opts=sync,strictatime

; skip these devices
; available tags: LABEL, PARTLABEL, UUID, PARTUUID, ID
; env: WDSIZE_SKIP_DEVICES
skip_devices=/dev/sda7,LABEL=WinCrap,UUID=66939eb1-2aca-41ed-8693-f4cef2bb81f9

; per-directory rules, only processed for matching dirs
; by default, all strings are parsed as glob patterns
; strings starting with a colon (:) are parsed as POSIX extended regex
; non-regex rules accept multiple dirs per rule, using the colon as a delimiter
[/:/usr/bin]
format=%m: (%p%% free) %B/%a

[~/downloads/torrents:~/var/rom]
recursive=1
output_format=r:%B/%a

[:^/home]
format=user's home %B/%a

; color scheme definition
; $color:<scheme name>
; each key is one or more terminal names (comma-separated);
; value is a list of attributes like bold, underscore, hidden etc. (comma-separated),
; followed by a semicolon with the actual colors, either by name or by code
; "?" is the catchall definition that applies to terminals not in the list
; a key with an empty value means no color for the specified terminal
; env: WDSIZE_COLOR (which also accepts the '<scheme name>:<source file>'
; format)
[$color:default]
?=bold;yellow,black
vte-256color=
xterm-256color=49,88
xterm,linux=bold;yellow,blue

; other tags: $fs, $gid, $uid, $user, $group, $term
; e.g. [$fs:ext4] will only apply to folders on an ext4 file system
