/*
 * xstat.c
 *
 * Copyright (C) Richard Ferreira
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#ifndef _DEFAULT_SOURCE
# define _DEFAULT_SOURCE
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <ftw.h>
#include <dirent.h>
#include "error.h"
#include "xstat.h"
#include "strlib.h"
#include "hash.h"
#include "error.h"
#include <sys/statvfs.h>
#include <mntent.h>

struct xstat *xb;
htable htab;
int (*stat_func)(const char *, struct stat *) = lstat;
long pathmax;

struct inodev {
	dev_t dev;
	ino_t ino;
};

static size_t idb_hash(const void *const key, size_t size)
{
	const struct inodev *idb = key;

	return (idb->dev ^ idb->ino) % size;
}

static int idb_found(const void *const idb1, const void *const idb2)
{
	const struct inodev *i1 = idb1, *i2 = idb2;

	return (i1->ino == i2->ino && i1->dev == i2->dev);
}

static int idb_new(const struct stat *const st)
{
	struct inodev *idb;
	int e;

	if (!(idb = malloc(sizeof(*idb))))
		error(EXIT_FAILURE, errno, "%s", __func__);
	idb->dev = st->st_dev;
	idb->ino = st->st_ino;

	if ((e = htable_insert(htab, idb)) < 0 || e == 1)
		free(idb);
	if (e < 0)
		error(0, errno, "%s", __func__);
	return e;
}

static void xstat_counter(const struct stat *const sb, const char *const filename)
{
	if (sb->st_nlink > 1 && idb_new(sb)) {
		xb->xt_nhlnk++;
		return;
	}
	switch (sb->st_mode & S_IFMT) {
	case S_IFSOCK:
		xb->xt_nsock++;
		break;
	case S_IFLNK:
		xb->xt_nlnk++;
		break;
	case S_IFREG:
		xb->xt_nreg++;
		break;
	case S_IFBLK:
		xb->xt_nblk++;
		break;
	case S_IFDIR:
		/* allow stat() on those but don't count them */
		if (strcmp(filename, "."))
			xb->xt_ndir++;
		break;
	case S_IFCHR:
		xb->xt_nchr++;
		break;
	case S_IFIFO:
		xb->xt_nfifo++;
		break;
	default:
		break;
	}
	xb->xt_ntotal = xb->xt_nsock + xb->xt_nlnk + xb->xt_nreg + xb->xt_nblk + xb->xt_ndir + xb->xt_nchr + xb->xt_nfifo;
	xb->xt_ndtotal = xb->xt_nsock + xb->xt_nlnk + xb->xt_nreg + xb->xt_nblk + xb->xt_nchr + xb->xt_nfifo;
	xb->xt_blocks += sb->st_blocks;
	xb->xt_size += sb->st_size;
}

static int xstat_filter(const char *const fpath, const struct stat *const sb, int tflag, struct FTW *ftw)
{
	if (tflag == FTW_DNR || tflag == FTW_NS) {
		error(0, errno, "%s", fpath);
		return 0;
	}
	xstat_counter(sb, fpath + ftw->base);
	return 0;
}

long get_pathmax(const char *const dirpath)
{
	long pathmax;

	if ((pathmax = pathconf(dirpath, _PC_PATH_MAX)) < 0) {
		if (errno) {
			error(0, errno, "%s: pathconf: %s", __func__, dirpath);
			return -1;
		}
		pathmax = _POSIX_PATH_MAX;
	}
	return pathmax;
}

static int xstat_chdir(const char *const dirpath)
{
	if (chdir(dirpath) < 0) {
		error(0, errno, "%s: chdir: %s", __func__, dirpath);
		return -1;
	}
	return 0;
}

static int xstat_readdir(const char *dirpath, int do_chdir)
{
	DIR *dir;
	struct dirent *de;
	struct stat sb;
	char *buf;
	char *cwd = NULL;
	const char *actual_dirpath = NULL;
	int ret = 0;

	actual_dirpath = dirpath;
	if (do_chdir) {
		cwd = malloc(pathmax + 1);
		getcwd(cwd, pathmax);
		if ((ret = xstat_chdir(dirpath)) < 0)
			goto end;
		actual_dirpath = ".";
	}
	if (!(dir = opendir(actual_dirpath))) {
		error(0, errno, "%s: opendir: %s", __func__, dirpath);
		return -1;
	}
	buf = malloc(pathmax + 1);
	while ((de = readdir(dir))) {
		if (!strcmp(de->d_name, ".."))
			continue;
		snprintf(buf, pathmax, "%s/%s", actual_dirpath, de->d_name);
		if (stat_func(buf, &sb) < 0) {
			error(0, errno, "%s: stat: %s", __func__, buf);
			continue;
		}
		xstat_counter(&sb, de->d_name);
	}
	closedir(dir);
	free(buf);
	if (do_chdir)
		ret = xstat_chdir(cwd);
end:
	free(cwd);
	return ret;
}

struct xstat *xstat(const struct xtconfig *const config)
{
	char *subdir;
	int state = 0;
	int ftwflags;

	ftwflags = FTW_PHYS;
	if ((pathmax = get_pathmax(config->dirpath)) < 0)
		return NULL;
	if (config->flags & XSTAT_FOLLOW_SYMLNKS) {
		stat_func = stat;
		ftwflags &= ~FTW_PHYS;
	}
	if (config->flags & XSTAT_XDEV)
		ftwflags |= FTW_MOUNT;
	if (config->flags & XSTAT_CHDIR)
		ftwflags |= FTW_CHDIR;
	if (!(htab = htable_init(HTABLE_SIZE, idb_hash, idb_found, free))) {
		error(0, 0, "%s: could not allocate hash table", __func__);
		return NULL;
	}
	xb = malloc(sizeof(*xb));
	memset(xb, 0, sizeof(*xb));
	xb->flags = config->flags;
	if ((state = stat(config->dirpath, &xb->sb)) < 0) {
		error(0, errno, "%s: stat: %s", __func__, config->dirpath);
		goto end;
	}
	if (config->flags & XSTAT_RECURSIVE) {
		if ((state = nftw(config->dirpath, xstat_filter, _POSIX_OPEN_MAX, ftwflags)) < 0)
			error(0, errno, "%s: nftw: %s", __func__, config->dirpath);
		goto end;
	}
	if ((state = xstat_readdir(config->dirpath, config->flags & XSTAT_CHDIR)) < 0) {
		goto end;
	}
	if (config->flags & XSTAT_SUBDIRS) {
		subdir = NULL;
		while ((subdir = argz_get(config->subdirs, config->subdir_count, subdir))) {
			if (access(subdir, F_OK) != 0) {
				if (errno == ENOENT && config->flags & XSTAT_SUBDIRS_ENOENT)
					error(0, errno, "%s: %s", __func__, subdir);
				continue;
			}
			xstat_readdir(subdir, config->flags & XSTAT_CHDIR);
		}
	}
end:
	if (state != 0) {
		free(xb);
		xb = NULL;
	}
	htable_fini(htab);
	return xb;
}

struct fsinfo *get_fsinfo(const char *const dirpath)
{
	struct fsinfo *ret = NULL;
	struct stat dpstat, mdstat;
	struct statvfs *vfs;
	struct mntent *me;
	FILE *fmtab;

	vfs = calloc(1, sizeof(*vfs));
	if (statvfs(dirpath, vfs) < 0) {
		free(vfs);
		vfs = NULL;
	}
	if (!(fmtab = setmntent("/etc/mtab", "r"))) {
		error(0, errno, "%s: /etc/mtab", __func__);
		return NULL;
	}
	if (stat(dirpath, &dpstat) < 0) {
		error(0, errno, "stat: %s", dirpath);
		return NULL;
	}
	while ((me = getmntent(fmtab)) && !ret) {
		if (stat(me->mnt_dir, &mdstat) < 0) {
			if (errno != EACCES)
				error(0, errno, "%s: %s", __func__, me->mnt_dir);
			continue;
		}
		if (dpstat.st_dev == mdstat.st_dev) {
			if (!(ret = calloc(1, sizeof(*ret)))) {
				error(0, ENOMEM, "%s", __func__);
				return NULL;
			}
			/* there's a slight difference between the percentage reported
			 * here and GNU's df (about 1-2%), which is found in other utilities
			 * as well */
			if (vfs) {
				ret->free_percent = (float)vfs->f_bavail / vfs->f_blocks * 100.;
				ret->used_percent = 100. - ret->free_percent;
				ret->b_total = vfs->f_blocks * vfs->f_frsize;
				ret->b_used = (vfs->f_blocks - vfs->f_bfree) * vfs->f_frsize;
				ret->b_free = vfs->f_bavail * vfs->f_frsize;
			}
			ret->mountpoint = strdup(me->mnt_dir);
			ret->fstype = strdup(me->mnt_type);
			ret->devpath = strdup(me->mnt_fsname);
			ret->fsopts = strdup(me->mnt_opts);
		}
	}
	endmntent(fmtab);
	free(vfs);
	return ret;
}

void fifree(struct fsinfo **f)
{
	if (!*f)
		return;
	if ((*f)->devpath)
		free((*f)->devpath);
	if ((*f)->mountpoint)
		free((*f)->mountpoint);
	if ((*f)->fstype)
		free((*f)->fstype);
	if ((*f)->fsopts)
		free((*f)->fsopts);
	free(*f);
}
