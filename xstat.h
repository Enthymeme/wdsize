/*
 * xstat.h
 * 
 * Copyright (C) Richard Ferreira
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR THE COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH
 * THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * Except as contained in this notice, the name(s) of the above copyright
 * holders shall not be used in advertising or otherwise to promote the sale,
 * use or other dealings in this Software without prior written authorization.
*/

#ifndef XSTAT_H
#define XSTAT_H

#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define HTABLE_SIZE	3137

#define XSTAT_RECURSIVE		(1 << 0)
#define XSTAT_XDEV		(1 << 1)
#define XSTAT_FOLLOW_SYMLNKS	(1 << 2)
#define XSTAT_CHDIR		(1 << 3)
#define XSTAT_SUBDIRS		(1 << 4)	/* include subdirs but doesn't recurse into them */
#define XSTAT_SUBDIRS_ENOENT	(1 << 5)	/* reports non-existent subdirs */

struct fsinfo {
	char *devpath;
	char *mountpoint;
	char *fstype;
	char *fsopts;
	uint64_t b_total;
	uint64_t b_used;
	uint64_t b_free;
	float used_percent;
	float free_percent;
};

struct xstat {
	struct stat sb;
	int flags;
	off_t xt_size;
	blkcnt_t xt_blocks;
	uint64_t xt_nreg,
		 xt_ndir,
		 xt_nchr,
		 xt_nblk,
		 xt_nfifo,
		 xt_nlnk,
		 xt_nsock,
		 xt_nhlnk,
		 xt_ntotal,
		 xt_ndtotal;
};

struct xtconfig {
	const char *dirpath;
	int flags;
	char *subdirs;
	size_t subdir_count;
};

struct xstat *xstat(const struct xtconfig *const config);
long get_pathmax(const char *dirpath);
void fifree(struct fsinfo **f);
struct fsinfo *get_fsinfo(const char *);

#endif
